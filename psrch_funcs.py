#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
psearch_funcs (psearch)
9 september 2009
"""
#
#*0*   *1*      *2*   3                         *4*
#File   scan    MS  proteina_original_Bioworks  masa
# *     *       *   reference                   actual_mass
#
#*5*                        6       7                  *8*
#deltya mass	            z	    Peptido Bioworks	Prob	
#(actual_mass-input_mass)   charge  peptide             ?
#
#Xcor	DCn	Sp 	Rsp Ions    count	D
#xcorr  deltacn rsp ions    count
#
#
import time
import os
import pythoncom
import xml.etree.ElementTree as et
from xml.parsers.expat import ExpatError
from psrch_dale_funcs import read_dict_from_list
from psrch_dale_funcs import find_best_line, calc_fdr
from psrch_dale_funcs import add_ms2_ms3_from_list
from psrch_dale_funcs import filter_identifications
from psrch_raw_class import RawFileExtractor, MockExtractor
from psrch_d_value import get_dvalue, count_aas
from psrch_quant import add_quant_from_raw
from commons.warns import tell
#
string = type('str')
#
#
def get_element(match, tag):
    """"""
    element = match.find(tag)
    try:
        element_text = element.text
        assert(type(element_text) is string)
    except (AttributeError, AssertionError):
        element_text = ""
    return element_text
#
#
def excel_from_xml(xml, raw):
    """"""
    xmltags = ['reference', 'actual_mass', 'charge', 'peptide', 
           'xcorr', 'deltacn', 'sp', 'rsp', 'count']
    #
    #                   1       2           
    tags = ['File', 'scan', 'ms_level', 'reference', 
    #                        5                             8
            'actual_mass', 'dmass', 'charge', 'peptide', 'Prob', 
    #                                         13             15
            'xcorr', 'deltacn', 'sp', 'rsp','ions', 'count', 'D']
    #
    filename = os.path.split(xml)[1]
    filename = filename.rsplit('.', 1)[0]
    #
    #print filename
    #time.sleep(1)
    #
    re = MockExtractor() if raw is None else RawFileExtractor(raw)
    #
    if not re.success:
        tell("raw_file <%s> could not be read" % raw)
        re = MockExtractor()
    #
    try:
        doc = et.parse(xml)
    except ExpatError, e:
        tell("file <%s> produces error\n'%s'" %(xml, e))
        return [tags]
    #
    matches = doc.findall("peptide_match")
    #
    data = [tags]
    for match in matches:
        line = [filename] + [get_element(match, tag)  for tag in xmltags]
        #
        #scan
        #Parent search must work with PQD and normal scans
        #PQD    -> ms2 = "1120 - 1123"  ms3 = "1124"  (programa 3PQD+1CID+1MS3)
        #Normal -> ms2 = "1123"         ms3 = "1124"
        #always happens that  scan.split(ms2)[-1] = "1123"
        scan = get_element(match, 'scan_range')
        scan = scan.split(" - ")[-1]
        #
        #ms_level
        ms_level = re.get_ms_level(int(scan))
        #
        #delta_mass
        actual_mass = line[2]
        input_mass = get_element(match, 'input_mass')
        delta_mass = float(actual_mass) - float(input_mass)
        #
        #ions
        ions = get_element(match, 'ions')
        try:
            ions = "%.1f" % eval("1.0 *" + ions)
        except ZeroDivisionError:
            ions = "0"
        #
        #D-value
        peptide, xc, dcn, dummy, rsp = line[4:9]
        #
        lp = count_aas(peptide)
        d_value = "%.3f" % get_dvalue(float(xc), float(dcn), float(rsp), delta_mass, lp)
        #
        line.insert(1, scan)
        line.insert(2, str(ms_level))
        line.insert(5, str(delta_mass))
        line.insert(8, "0")
        line.insert(13, ions)
        line.append(d_value)
        data.append(line)
    
    re.close()
    del re
    return data
#
#
def get_data_ms2_ms3(alist, clave='decoy'):
    """Recovers the data for all pairs ms3-ms2 pointing to the same sequence.

    The methods is based on the observed high confidence of these
    coincident matches, even despite showing a low score and then being
    filtered out in other steps.

    - Classify points in a dictionary <basket>
    basket[id] = (protein, idx)
                        where id = (file, scan, level, carga)
                        where idx is the index in <lista> (the file number)
    - Then search for ms3 scans.
    - When one is found it checks for the corresponding ms2 scan
    - If ms2 and ms3 point to the same identification, saves both in a list

    """
    basket = {}
    list_ms2_ms3 = []
    for idx, match in enumerate(alist):
        try:
            if clave in match[3]: continue
        #if match[3] is not string or doesnt exists
        except (TypeError, IndexError):
            continue
        #
        # id_ = (file, scan, level, carga)
        id_ = (match[0], match[1], match[2], match[6])  
        basket[id_] = (match[3], idx)
        
    for file_, scan, level, charge in basket:
        if level == 'ms3':
            ms3_prot, ms3_idx = basket[(file_, scan, level, charge)]
            ms2_scan = str(int(scan) -1)
            for z in ['1', '2', '3']:
                try:
                    ms2_prot, ms2_idx = basket[(file_, ms2_scan, 'ms2', z)]
                except KeyError:
                    continue
                #
                if ms2_prot == ms3_prot:
                    list_ms2_ms3.append(alist[ms2_idx])
                    list_ms2_ms3.append(alist[ms3_idx])
    
    return list_ms2_ms3
#
#
def load_list_of_list(archive):
    """"""
    return [line.rstrip().split('\t') for line in open(archive, 'r')]
#
#
def save_list_of_lists(alist, archive="C:/outtext.xls"):
    """"""
    data = []
    for item in alist:
        #print item
        try:
            text = "\t".join(item)
            data.append(text)
        except TypeError:
            continue
    
    save_txt_in_file(data, archive)
#
#
def save_dict_of_lists(diccio, arch="C:/outtext_2.xls", headlines=None):
    """"""
    header = ["\t".join(headlines)] if headlines else []
    lista = header + ["\t".join(item) for item in diccio.values()]
    
    save_txt_in_file(lista, arch)
#
#
def save_txt_in_file(text, filepath):
    """Save text in <text> in file <filepath>.

    <text> can be either a string or a list of strings

    """
    #
    if type(text) is list:
        while True:
            try:
                if os.path.exists(filepath): os.remove(filepath)
                break
            except WindowsError:
                tell("Close the document <%s> before continuing" % filepath)
            
        hndl = open(filepath, 'a')
        size = 20000        # seems that with 55000 lines already crashes.
        index = 0
        while index < len(text):
            #print "chunk ", index
            new_index =  index + size   
            fulltext = "\n".join(text[index:new_index])
            hndl.write(fulltext + '\n')
            index = new_index
        hndl.close()
    else:
        while True:
            try:
                open(filepath, 'w').write(text)
                break
            except IOError:
                tell("Close the document <%s> before continuing" % filepath)
#
#
def get_next_xml_raw(xml, raw):
    """"""
    #
    at_least_one = False
    #
    if os.path.isfile(xml) and os.path.isfile(raw):
        #print "isfile"
        yield [xml, raw]
    elif os.path.isdir(xml) and os.path.isdir(raw):
        #print "isdir"
        for fxml in os.listdir(xml):
            fxml_path = os.path.join(xml, fxml)
            if os.path.isfile(fxml_path) and fxml_path.endswith('.xml'):
                fraw = fxml[:-3] + "raw"
                fraw_path = os.path.join(raw, fraw)
                at_least_one = True
                if os.path.exists(fraw_path):
                    yield (fxml_path, fraw_path)
                else:
                    yield (fxml_path, None)  #continue
            else:
                continue
        if not at_least_one:
            tell('There are not valid files in <%s>' % xml)
            yield None, None
    else:
        tell('<%s> or <%s> do not exist' %(xml, raw))
        yield None, None
#
#
#noinspection PyUnusedLocal
def get_next_xml_mock(xml, raw):
    """Does not use the RAW file"""
    #
    if os.path.isfile(xml):
        #print "isfile"
        yield [xml, None]
    elif os.path.isdir(xml):
        #print "isdir"
        for archive in os.listdir(xml):
            xml_path = os.path.join(xml, archive)
            if os.path.isfile(xml_path) and xml_path.endswith('.xml'):
                yield (xml_path, None)
            else:
                continue
    else:
        tell('<%s> does not exist' % xml)
#
#
def run_from_xml_raw(self):
    """"""
    #psyco.full()
    #noinspection PyUnresolvedReferences
    pythoncom.CoInitialize()
    #
    #t0 = time.time()
    #
    headlines = None
    xml = self.xml
    raw = self.raw
    xls = self.xls
    fxls = self.fxls
    basedir, arch = os.path.split(xls)
    #
    self.classes = ['tar', 'dec']
    #
    log = self.tc_sec.WriteText
    #
    log("%s\n%s\n" %(xml, raw))  
    #
    provider = get_next_xml_raw if self.use_raw else get_next_xml_mock
    #
    #calculate common fdr cutoff
    #noinspection PySimplifyBooleanCheck
    if self.property['oper'] == 0:
        #
        data = []
        n = 0
        for xml_path, raw_path in provider(xml, raw):
            if xml_path is None:
                log("Failed Process\n")
                self.maniclock.Destroy()
                return
            #
            n += 1
            log('file %i --> %s\n' % (n, os.path.basename(xml_path)))
            part_data = excel_from_xml(xml_path, raw_path)
            if n == 1:
                data.extend(part_data)
            else:
                data.extend(part_data[1:])
        
        #save the complete data
        log("*** saving the full report <%s> ***\n" % fxls)
        if self.property['sorg']: save_list_of_lists(data, fxls)
        self.bt_see_fxls.SetBackgroundColour("green")
        #data is destroyed in common_process
        dict_selected, headlines = common_process(self, data)
    #
    #calculate fdr cut per file
    else:
        #
        full_data = []
        dict_selected = {}
        n = 0
        for xml_path, raw_path in provider(xml, raw):
            n += 1
            log('file %i --> %s\n' %(n, os.path.basename(xml_path)))
            data = excel_from_xml(xml_path, raw_path)
            if n == 1:
                full_data.extend(data)
            else:
                full_data.extend(data[1:])
            partial_dict_selected, headlines = common_process(self, data)
            dict_selected.update(partial_dict_selected)

        #save full report
        log("*** saving the full report <%s> ***\n" %fxls)
        if self.property['sorg']: save_list_of_lists(full_data, fxls)
        self.bt_fxls.SetBackgroundColour("green")
    #
    #save result output file
    log("*** saving results in <%s> ***\n" %xls)
    save_dict_of_lists(dict_selected, xls, headlines)
    #
    #save log file
    my_time = time.localtime(time.time())
    logfile = '%i_%i_%i_%i_%i_%i.log' % my_time[:6]
    path = os.path.join(basedir, logfile)
    text = self.tc_sec.GetValue()
    log("*** saving log file in <%s> ***\n" %path)
    save_txt_in_file(text, path)
    #
    self.bt_see_xls.SetBackgroundColour("green")
    #
    self.maniclock.Destroy()
    #
    #t1 = time.time()
    #print 'time= ', t1-t0
    #25sec no psyco
    #19.4sec    
#
#
def run_from_datafile(self):
    """"""
    #noinspection PyUnresolvedReferences
    pythoncom.CoInitialize()
    #
    #t0 = time.time()
    #
    xls = self.xls
    basedir, arch = os.path.split(xls)
    datafile = self.fxls
    #
    log = self.tc_sec.WriteText
    #
    log("%s\n" % datafile)
    #
    #read data from datafile
    log("*** reading data from result file <%s> ***\n" %datafile)
    data = load_list_of_list(datafile)
    #process data. data is destroyed in common_process
    dict_selected, headlines = common_process(self, data)
    #
    #save output file
    log("*** saving results in <%s> ***\n" %xls)
    save_dict_of_lists(dict_selected, xls, headlines)
    #
    #save log file
    my_time = time.localtime(time.time())
    logfile = '%i_%i_%i_%i_%i_%i.log' % my_time[:6]
    path = os.path.join(basedir, logfile)
    text = self.tc_sec.GetValue()
    log("*** saving log file in <%s> ***\n" %path)
    save_txt_in_file(text, path)
    #
    self.bt_see_xls.SetBackgroundColour("green")
    #
    self.maniclock.Destroy()
    #
    #t1 = time.time()
    #print 'time= ', t1-t0
    #25sec no psyco
    #19.4sec    
#
#
def common_process(self, data):
    """"""
    #
    log = self.tc_sec.WriteText
    columns = (1, 2, 7, 4)           #['File', 'scan', 'charge','reference']
    fdr = float(self.property['cut_fdr'])
    raw = self.raw
    #print "raw in common ", raw
    start, bound, adjust, extra = self.fdr_line
    #
    #start dale functions
    self.points = read_dict_from_list(data[1:], self.td_key)
    #
    #calculate parameters for filtering and collection of selected points
    log("*** calculating filtering parameters for fdr < %.2f ***\n" % fdr)
    t0 = time.time()
    px, py, ntar, ndec, self.selected = find_best_line(fdr, self.points, start,
                                                        bound, adjust, extra)
    t1 = time.time()
    log("      -> time = %.3f sec <-\n" %(t1-t0))
    #
    self.fdr_params = (ntar, ndec)
    self.fdr_line = ((px, py),) + self.fdr_line[1:]
    new_fdr = calc_fdr(ntar, ndec)
    log("Dvalue= %.1f  Xcor= %.1f    #target= %i  #decoy= %i\n" % (px, py, ntar, ndec))
    #
    #prepare the new file only with new data
    log("*** filtering data ***\n")
    dict_selected = filter_identifications(data, self.selected, self.td_key, columns=columns)
    log("%i filtered points with fdr = %.2f\n" %(len(dict_selected), new_fdr))
    #
    #add ms2_ms3
    if self.property['2_3']:
        log("*** calculating ms2_ms3 ***\n")
        data_ms2_ms3 = get_data_ms2_ms3(data, self.td_key)
        #
        dict_selected = add_ms2_ms3_from_list(data_ms2_ms3,  
                        dict_selected, headlines=(1,), columns=columns)
        log("%i points including ms2_ms3\n" %(len(dict_selected),))
    #
    #add quantification iTRAQ_TMT
    headlines = data[0]
    #
    if self.property['quant']:
        #delete data trying to get more memory
        data[:] = []
        import gc
        gc.collect()
        time.sleep(2)
        #comienzo
        t0 = time.time()
        origin, isodata = self.isocurrent[self.reagent]
        nrgnt = len(isodata)
        
        log("*** adding data <%s> ***\n" % self.reagent)
                
        reagent_h = ["%s_%i" % (self.reagent, idx+1)  for idx in xrange(nrgnt)]
        quant_h = ["quant_%i" % (idx+1)  for idx in xrange(nrgnt)]
        headlines += reagent_h + quant_h
        
        log("*** quantifying with table <%s> ***\n" % origin)

        log("    -> pqd_number %i, ref_pos = %i<-\n" % self.scandata)
        #
        dict_selected = add_quant_from_raw(dict_selected, raw,
                                           isodata, self.calidata,
                                           self.scandata, self.reagent)
        #
        t1 = time.time()
        log("      -> time = %.3f sec <-\n" %(t1-t0))
    #
    #quit
    
    return dict_selected, headlines


if __name__ == '__main__':
    #   
    print "use script_search for test"
    raw_input("enter to quit")