#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
psrch_aux_guis (psearch 106)
1 september 2009
"""
#
import wx
import os
#
from psrch_class_iso import IsoFrame
from xtraq_class_cal import CaliFrame
from psrch_class_smode import ScanCycleFrame
from psrch_txt import CABECERA, HEAD_SMODE
#
#
#noinspection PyUnusedLocal
class ScanCycle(ScanCycleFrame):
    def __init__(self, parent, scandata, *args, **kargs):
        ScanCycleFrame.__init__(self, parent, *args, **kargs)
        self.tc_head.SetValue(HEAD_SMODE)
        self.parent = parent
        self.set_data(scandata)
        self.Bind(wx.EVT_BUTTON, self.on_bt_apply, self.bt_apply)
        self.Bind(wx.EVT_BUTTON, self.on_bt_exit, self.bt_exit)
        
    def set_data(self, data):
        pqd_number, ref_pos = data
        scan_method = pqd_number - ref_pos
        self.sp_npqd.SetValue(pqd_number)
        self.rbx_scmthd.SetSelection(scan_method)
    
    def get_data(self):
        pqd_number = self.sp_npqd.GetValue()
        scan_method = self.rbx_scmthd.GetSelection()
        ref_pos = pqd_number - scan_method
        return pqd_number, ref_pos
        
    def on_bt_exit(self, evt):
        self.Destroy()
    
    def on_bt_apply(self, evt):
        data = self.get_data()
        self.parent.scandata = data
#
#
#noinspection PyUnusedLocal
class CaliQ(CaliFrame):
    def __init__(self, parent, calidata, *args, **kwargs):
        CaliFrame.__init__(self, parent, *args, **kwargs)
        self.label_1.SetValue(CABECERA)
        self.parent = parent
        self.set_data(calidata)
        self.Bind(wx.EVT_BUTTON, self.on_bt_apply, self.bt_apply)
        self.Bind(wx.EVT_BUTTON, self.on_bt_exit, self.bt_exit)
        
    def set_data(self, data):
        self.sp_cntr.SetValue(data[0])
        self.sp_wndw.SetValue(data[1])
    
    def get_data(self):
        center = self.sp_cntr.GetValue()
        width = self.sp_wndw.GetValue()
        return center, width
        
    def on_bt_exit(self, evt):
        self.Destroy()
    
    def on_bt_apply(self, evt):
        data = self.get_data()
        self.parent.calidata = data
#
#
#noinspection PyUnusedLocal
class IsoQ(IsoFrame):
    def __init__(self, parent, reagent, data, origin, *args, **kwargs):
        """"""
        self.gsize = len(data)
        IsoFrame.__init__(self, parent, self.gsize, *args, **kwargs)
        self.parent = parent
        self.reagent = reagent
        self.origin = origin
        self.__set_data(data)
        title = "ISOTOPE TABLE -- %s" % origin
        self.SetTitle(title)
        self.Bind(wx.EVT_BUTTON, self.__on_bt_apply, self.bt_apply)
        self.Bind(wx.EVT_BUTTON, self.__on_bt_exit, self.bt_exit)
        self.Bind(wx.EVT_BUTTON, self.on_bt_save, self.bt_save)
        self.Bind(wx.EVT_BUTTON, self.on_bt_load, self.bt_load)
        self.Bind(wx.EVT_BUTTON, self.on_bt_svdflt, self.bt_svdflt)

    #noinspection PyArgumentList
    def __set_data(self, data):
        """"""
        for row in xrange(len(data)):
            for column in xrange(len(data[0])):
                self.grd.SetCellValue(row, column, '%.3f' %(data [row][column]))
    #
    #noinspection PyArgumentList
    def __get_data(self):
        """"""
        gcv = self.grd.GetCellValue
        data = [[float(gcv(row, column)) for column in xrange(self.gsize)]
                                          for row in xrange(self.gsize)]
        return data
    #
    def __on_bt_exit(self, evt):
        self.Destroy()
    #
    def __on_bt_apply(self, evt):
        """"""
        data = self.__get_data()
        title = self.GetTitle()
        if 'CLICK' in title: title = title[0:-13]
        self.SetTitle(title)
        origin = title[17:]
        self.parent.isocurrent[self.reagent] = [origin, data]
    #
    def on_bt_svdflt(self, evt):
        """"""
        savepath = self.parent.def_dir + os.sep + 'default.' + self.reagent
        self.__save(savepath)
        title = "ISOTOPE TABLE -- %s  (CLICK APPLY)" % 'default'
        self.SetTitle(title)
    #
    def on_bt_save(self, evt):
        """"""
        #
        dlg = wx.FileDialog(None, "Select File",
                                defaultDir = self.parent.sdef_dir,
                                wildcard= "isotope files|*." + self.reagent,
                                style=wx.SAVE|wx.OVERWRITE_PROMPT)
        #
        if dlg.ShowModal() == wx.ID_OK:
            save_path  = dlg.GetPath()
            if save_path: self.__save(save_path)
    #
    def __save(self, save_path):
        """"""
        dummy, file = os.path.split(save_path)
        data = self.__get_data()
        text = ''
        for item in data:
            txtlist = [str(number) for number in item]
            text += '\t'.join(txtlist) + '\n'
        open(save_path, 'w').write(text)
        self.parent.sdef_dir = save_path
        title = "ISOTOPE TABLE -- %s  (CLICK APPLY)" % file
        self.SetTitle(title)
    # 
    def on_bt_load(self, evt):
        """"""
        dlg = wx.FileDialog(None, "Select File",
                                defaultDir=self.parent.sdef_dir,
                                wildcard= "isotope files|*." + self.reagent,
                                style=wx.SAVE)
        
        if dlg.ShowModal() == wx.ID_OK:
            load_path  = dlg.GetPath()
            #
            data = []
            for line in open(load_path, 'r'):
                row = [float(item) for item in line.split()]
                data.append(row)
            self.__set_data(data)
            self.parent.sdef_dir = load_path
            dummy, file = os.path.split(load_path)
            title = "ISOTOPE TABLE -- %s   (CLICK APPLY)" % file
            self.SetTitle(title)
#
#
class MyFrame(wx.Frame):
    def_dir= "C:/"
    sdef_dir= "C:/"
#
#
class MyApp(wx.App):
    def OnInit(self):
        from psrch_constants import iso_itq, iso_tmt
        from psrch_constants import calidata, scandata
        wx.InitAllImageHandlers()
        smodeq = ScanCycle(None, scandata)
        caliq = CaliQ(None, calidata)
        mf = MyFrame(None)
        isoq_4 = IsoQ(mf, 'itraq', iso_itq, "mock")
        isoq_6 = IsoQ(mf, 'tmt', iso_tmt, "mock")
        smodeq.Show()
        caliq.Show()
        isoq_4.Show()
        isoq_6.Show()
        return 1
#
#
if __name__ == "__main__":
    
    app = MyApp(0)
    app.MainLoop()
