#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
psrch_dale_funcs (psearch)
15 january 2010

I have two files:
 One with good identifications -> good_points
 another with bad ones         -> bad_points

each identification (point) has two parameters x,y (scores)
I have to write a function f allowing to select those points for which
f(x,y) > cutoff. Then I have to optimize the function to maximize the number
of good_points that pass when a restriction of FDR < value (p.e 2) is set.
FDR = bad_points/total_points

pb could be a dict pb[id]=(Xcor, D, klass)
"""
#
try:
    import psyco
except ImportError:
    from psrch_constants import MockPsyco
    psyco = MockPsyco()
#
import random
from collections import defaultdict
from commons.warns import tell
#
#
#
def add_ms2_ms3_from_list(alist, dict_selected, headlines=None, columns=None):
    """"""
    col_idxs = [col-1 for col in columns[0:3]]   #'File', 'scan', 'charge'   
    error = 0
    line = 0
    for data in alist:
        line += 1
        try:
            ids = [data[idx] for idx in col_idxs]
        except (IndexError, ValueError):
            if error < 10:                 #warn only in fists 10 failures
                #noinspection PyUnresolvedReferences,PyUnboundLocalVariable
                tell("Error in column <%s> in line <%s>" % (idx, data))
                error += 1
        else:
            id_ = '|'.join(ids)
            if (id_ not in dict_selected) and (line not in headlines):
                dict_selected[id_] = data
            
    return dict_selected
#
#
def filter_identifications(data, selected, mock_key, columns=None):
    """Builds a dictionary of selected target identifications

    <data> -> list of psearch identifications (list of lists)
    <selected> -> list of psearch point dictionary keys (ids)
    <mock_key> -> label added to the protein reference to distinguish decoy
                from target identifications (i.e. 'decoy')
    <columns> -> list indicating the positions of columns 'File', 'scan',
                'charge' and 'reference' in data rows

    """
    full_select = {}
    id_cols = [index-1 for index in columns[0:3]]   #'File', 'scan', 'charge' 
    ref_col = columns[3] - 1                        #'reference'
    #
    n = 0
    for row, values in enumerate(data):
        try:
            ids = [values[col] for col in id_cols]
        except (IndexError, ValueError):
            if n < 10:           # only warn on the 10 first failures
                #noinspection PyUnresolvedReferences,PyUnboundLocalVariable
                tell("Error in line <%i> column <%i>" % (row, col))
                n += 1
        else:
            id_ = '|'.join(ids)
            #print 'id', id_
            if id_ in selected and mock_key not in values[ref_col]:
                full_select[id_] = values
    #
    return full_select
#
#
def read_dict_from_list(alist, mock_key):
    """A list of lists with the following items:
    0       1     2     3                   4       5       6   7
    File    scan  MS    proteina_Bioworks   masa    d_mass  z   Peptido_Bioworks
    8       9       10  11  12  13      14      15
    Prob    Xcor    DCn Sp  Rsp Ions    count   D
    #
    Returns a dict of points
    points[id] = (Xcorr, D, klass) where id = "File|Scan|z"
    """
    points = {}
    for match in alist:
        #print  match
        protein = match[3]
        if protein:
            #print 'm2'
            scan, charge, xcor, D = match[1], match[6], match[9], match[15]
            filename = match[0]
            id_ = '|'.join([filename, scan, charge])
            #print protein
            klass = 'dec' if (mock_key in protein) else 'tar'
            points[id_] = (float(xcor), float(D), klass)
        else:
            continue         
    #print points.values()
    return points
#
#
#noinspection PyUnusedLocal
def select_line(xcut, ycut, points, extra=None):
    """xcut=dvalue, ycut=xcor"""
    selected = {}
    slope = ycut / xcut
    for item in points:
        xcor, d, t = points[item]
        y = ycut - slope * d
        if xcor > y:
            selected[item] = (xcor, d, t)
    return selected
#
#
#noinspection PyUnusedLocal
def count_line(xcut, ycut, points, extra=None):
    """xcut=dvalue, ycut=xcor"""
    count = defaultdict(int)
    slope = ycut/xcut
    for item in points:
        xcor, d, t = points[item]
        y = ycut - slope * d
        if xcor > y:
            count[t] += 1
    return count
#
#
def select_circle(x, y, points, radio):
    """Filters items on the basis of a circular boundary

    Maybe better select fixed radius or position determined  by regression.
    Warning : gives error with negative points.

    """
    selected = {}
    r2 = radio**2
    for item in points:
        xcor, d, t = points[item]
        if d > x:
            x2 = (d-x)**2
            y2 = (xcor-y)**2
            r2_point = x2 + y2
            if r2_point > r2:
                selected[item] = (xcor, d, t)
    return selected
#
#
def count_circle(x, y, points, radius):
    """Counts items outside a circular boundary.

    Maybe better select fixed radius or position determined  by regression.
    Warning : gives error with negative points.

    """
    count = defaultdict(int)
    r2 = radius**2
    for item in points:
        xcor, d, t = points[item]
        if d > x:
            x2 = (d-x)**2
            y2 = (xcor-y)**2
            r2_point = x2 + y2
            if r2_point > r2:
                count[t] += 1
    return count
#
#
def select_axes(x, y, points, trisectors=False):
    """Filter items on the basis of x and y cutoffs.

    x --> XCorr
    y --> D-value
    trisectors = True  --> (point_x > x) OR  (point_y > y)
               = False --> (point_x > x) AND (point_y > y)

    """
    selected = {}
    if trisectors:
        for item in points:
            xcor, d, t = points[item]
            if d > x or xcor > y:                   #or
                selected[item] = (xcor, d, t)
    else:
        for item in points:
            xcor, d, t = points[item]
            if d > x and xcor > y:                  #and
                selected[item] = (xcor, d, t)
        
    return selected
#
#
def count_axes(x, y, points, trisectors=False):
    """Counts items that pass <x> , <y> cutoffs"""
    count = defaultdict(int)
    if trisectors:
        for item in points:
            xcor, d, t = points[item]
            if d > x or xcor > y:                   #or
                count[t] += 1
    else:
        for item in points:
            xcor, d, t = points[item]
            if d > x and xcor > y:                  #and
                count[t] += 1
        
    return count
#
#
def calc_fdr(target, decoy):
    """False Discovery Rate calculation"""
    #
    try:
        return 100.0 * decoy / (decoy + target)
    except ZeroDivisionError:
        return 100
#
#
def calcula(x, y, window, btarget, bdecoy, fdr_max, func, points, extra):
    """
    btarget -> best target
    """
    bestx = x
    besty = y
    for i in range(1500):
        nx = x + 0.001 * random.randint(-window, window)
        ny = y + 0.001 * random.randint(-window, window)
        #
        count = func(nx, ny, points, extra)
        target = count['tar']
        decoy = count['dec']
        fdr = calc_fdr(target, decoy)
        #
        if fdr <= fdr_max:
            if target > btarget:
                bdecoy = decoy
                bestx = nx
                besty = ny
                btarget = target
                #print bestx, besty, besteff, btarget, bdecoy
    return bestx, besty, btarget, bdecoy         
#
#
def find_best_line(fdr_min, points, start=(7, 7), bound=0, adjust=1, extra=None):
    """Find best cutoff line for a given FDR.

    Currently it can use select_line, select circle (with a fixed radio of 9)
    or select_axes.
    For phosphopeptide Xcorr/D-value distributions, the best initial values are:
      line  -> x=5, y=5
      circle-> x=-1, y=-1 (depending on the radius)
      axes  -> no determined

    bound -> boundary type: lineal(0)/circle(1)/axes(2)
    adjust -> line parameters set are fix(0)/as_start points for iteration(1)
    extra  -> extra data: radius (crad:float) in case of circle boundary or
                         quadrants (a3s:bool) in case of axes boundary
    """
    psyco.full()
    #
    functions = [(select_line, count_line),
                 (select_circle, count_circle),
                 (select_axes, count_axes)]
    #    
    sectors = [4000, 2000, 500, 50]
    target = 0
    decoy = 0
    #
    x, y = start  #dvalue, #xcor
    func_count = functions[bound][1]
    func_select = functions[bound][0]
    #
    #noinspection PySimplifyBooleanCheck
    if adjust == 0:
        count = func_count(x, y, points, extra)
        target, decoy = count['tar'], count['dec']
    else:
        relevant = get_relevant(points)
        for win in sectors:
            x, y, target, decoy = calcula(x, y, win, target, decoy,
                                           fdr_min, func_count, relevant, extra)
    #
    selected = func_select(x, y, points, extra)
    return x, y, target, decoy, selected
#
#
def get_relevant(points, xlim= 2, ylim= 2):
    """Selects items above given <xlim>, <ylim> cutoff"""
    relevant = {}
    for item in points:
        xcor, d, t = points[item]
        if xcor > xlim or d > ylim:
            relevant[item] = (xcor, d, t)
    return relevant
#
#
def color(klass='dot'):
    """color generator for plotting"""
    if klass == "dot":
        colors = ['b.', 'r.', 'g.', 'y.']
    else:
        colors = ['b', 'r', 'g', 'y']
    for acolor in colors:
        yield acolor
#
#
#
if __name__ == '__main__':
    
    print
    print "lacks test function"
    print "use script_filtra_dale.py and script_dibuja_distribuciones_dale.py"
    print
    raw_input("enter to exit")
