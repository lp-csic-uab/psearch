#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
psrch_constants.py (psearch 160)
10 september 2009
"""
#
#
XMLTEST = r'C:\datos_psearch\test\xml'
RAWTEST = r'C:\datos_psearch\test\raw'
FILETEST = r'C:\datos_psearch\test\my_full_default.xls'
EXCELFILE = r'C:\datos_psearch\my_out_default.xls'
#
def_dir = r'C:\datos_psearch'
xmldef_dir = r'C:\datos_psearch\test\XML'
rawdef_dir = r'C:\datos_psearch\test\RAW'
#
#
CLAVE = 'decoy' 
#
#           R114   R115    R116    R117
iso_itq = [[1.000, 0.020, 0.000, 0.000],    #114
           [0.063, 1.000, 0.030, 0.000],    #115
           [0.002, 0.060, 1.000, 0.040],    #116
           [0.000, 0.001, 0.049, 1.000]     #117
          ]        
#
#           R126   R127    R128   R129   R130  R131
iso_tmt = [[1.000, 0.020, 0.000, 0.000, 0.00, 0.00],    #126
           [0.063, 1.000, 0.030, 0.000, 0.00, 0.00],    #127
           [0.002, 0.060, 1.000, 0.040, 0.00, 0.00],    #128
           [0.000, 0.001, 0.049, 1.000, 0.40, 0.00],    #129
           [0.000, 0.000, 0.020, 0.060, 1.00, 0.40],    #130
           [0.000, 0.000, 0.002, 0.040, 0.60, 1.00]     #131
        ]        
#
# data for reporter ion mass calibration
calidata = (0, 450)  #(offset, width)
#
#
scandata = (3, 3)  # (number of pqd scans, ref_pos)
#
# fdr line settings
linedata = dict(bound = 0,           #boundary type: lineal(0)/circle(1)/axes(2)
                adjust = 0,          #use values:    fix(0)/as_start(1)
                xc = '6',            #lineal : Xcorr initial value
                dval = '6',          #lineal : Dvalue initial value
                cxc = '1',           #circle : x-center coord initial value
                cyc = '1',           #circle : y-center coord initial value
                axc = '4',           #axes   : Xcorr initial value
                adval = '4',         #axes   : Dvalue initial value
                crad = '2',          #circle : radius
                a3s = True           #sectors: 1 quadrant(0)/3 quadrants(1)
                )               
#
# selected fdr line params
mylinedata = ((7,6),   #start dvalue, xcorr
               0,       #bound
               1,       #adjust
               None     #xtra (crad o a3s)
            )
#
# psyco mocking class
class MockPsyco():
    def full(self):
        pass
