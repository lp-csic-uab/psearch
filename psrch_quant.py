#!/usr/bin/env python
# -*- coding: latin-1 -*-
#
"""
psrch_quant (psearch)
1 september 2009
"""
#
import os
import time
from numpy import matrix
from scipy.linalg import solve
from psrch_constants import iso_itq, iso_tmt, calidata, scandata
from psrch_raw_class import RawExtractor, MockExtractor
from commons.warns import tell
#
class Qmasses():
    """A class to set and memorize (itraq and tmt) reporter ion actual masses

    Probably it is not given the exact reporter mass, because adding 1 unit to
     the base mass is not correct. should this be fixed ?

    """
    qmasses = None
    #
    @classmethod
    def set(cls, calidata, reagent):
        if reagent == 'itraq':
            n = 4
            base = 114.2
        else:
            n = 6
            base = 126.2    #set correct value
        #
        base += calidata[0]/1000.
        desv = calidata[1]/1000.
        base_min = base - desv
        base_max = base + desv

        cls.qmasses = [(idx, base_min+idx, base_max+idx) for idx in xrange(n)]
#
#
def get_qint_calculator(calidata, reagent):
    """Reporter ion intensity calculator Function factory.

    Returns a function that sums intensities of signals inside a mass
    window for each reporter ion. This factory presets the center and witdh
    of the mass window.

    """
    Qmasses.set(calidata, reagent)
    qmasses = Qmasses.qmasses

    min_mass = qmasses[0][1]
    max_mass = qmasses[-1][2]

    def get_qintensities(spectrum):
        """Sum intensities of signals inside a given mass window for each reporter ion"""
        #
        qint = [0] * len(qmasses)
        for intensity, mass in spectrum:
            if mass < min_mass: continue
            if mass >= max_mass: break                  # here faster
            #
            for index, minval, maxval in qmasses:
                if minval <= mass < maxval:
                    qint[index] += intensity
                    break
            #if massvalue >= maxmass: break                 # slower

        return qint
    return get_qintensities
#
#
def get_quant_calculator(isodata):
    """Reporter ion quantifier function factory"""
    A = matrix(isodata)
    def get_quant(intensities):
        """Correct contributions of isotopic signals to reporter ions."""
        #noinspection PyUnresolvedReferences
        b = matrix(intensities).transpose()
        my_matrix = solve(A, b).transpose()
        return ['%.1f' % item for item in my_matrix[0]]
    return get_quant
#
#
def add_quant_from_raw(diccio, raw, isodata=iso_itq, calidata=calidata,
                                       scandata=scandata, reagent='itraq'):
    """Add quantitative data from original raw file

    PQD scans can be single scan or n scans that have to be averaged.
    Common scan cycles are of the type:

    n pqdMS2 + 1 cidMS2 + 1 cidMS3  (phospho analysis with iTRAQ, n = 3)
               1 cidMS2 + 1 cidMS3  (analysis w/o quantification)
    n pqdMS2                        (general analysis, p.e. CENIT, n=1)
    
    For Bioworks identifications, MS2 spectra are summed and a scan series is
    assigned to the identification (p.e. "1000-1004" -> 3 pqd + 1 cid).
    Depending on the scan mode, the scans with sequence info can be the
    cidMS3 and/or cidMS2 (first and second case) or a pqdMS2 (third case).
    I extract always the higher scan ("1004") corresponding to cidMS2.
    This scan number is the one saved for the report.

    - raw can be a file or a directory.
    - scandata = (pqd_number, ref_pos) determines the number of PQD spectra to
    average for the reporter ion intensities calculation (n=pqd_number) and the
    position in the scan series of the last MS2 saved (ref_pos).
    Example:
      0      1        2         3
    pqdMS2 + pqdMS2 + pqdMS2 + cidMS2 + cidMS3  ->pqd_number=3, ref_pos=3
    pqdMS2                                      ->pqd_number=1, ref_pos=0
    pqdMS2 + pqdMS2                             ->pqd_number=2, ref_pos=1
    pqdMS2 + pqdMS2                   + cidMS3  ->pqd_number=2, ref_pos=1 ???
    To solve the last case requires searching the parent of cidMS3 tp have the
    MS2 of reference?? maybe no.

    """
    #
    num_reporters = 4 if reagent == 'itraq' else 6
    pqd_number, ref_pos = scandata
    #
    get_quant = get_quant_calculator(isodata)
    get_qint = get_qint_calculator(calidata, reagent)
    keys = diccio.keys()
    keys.sort()
    #
    raw_extractor = None
    if os.path.isfile(raw):
        path = raw
        raw_extractor = RawExtractor(path, True)
    
    name = ""
    for item in keys:
        #print item
        filename, scan, z = item.split('|')
        if os.path.isdir(raw) and filename != name:
            if raw_extractor: del raw_extractor
            name = filename
            raw_name = name + '.raw'
            path = os.path.join(raw, raw_name)
            #print path
            if os.path.isfile(path):
                time.sleep(1)
                raw_extractor = RawExtractor(path, True)
                if not raw_extractor.success:
                    tell("raw_file %s could not be read" % raw)
                    raw_extractor = MockExtractor()
            else:    
                raw_extractor = MockExtractor()
        #
        scan_number = int(scan)
        # 
        #ms_level will be taken from file from now on
        #ms_level = rex.get_ms_level(scan_number)
        ms_level = diccio[item][2]
        #
        if ms_level == "ms3":
            pqd_scan_1 = scan_number - ref_pos - 1
        elif ms_level == "ms2":
            pqd_scan_1 = scan_number - ref_pos
        else:
            continue
        #
        #intensity reporter ions
        qints = [0] * num_reporters
        for rel_index in range(pqd_number):
            #print pqd_scan_1 + rel_index
            spectrum = raw_extractor.get_ms(pqd_scan_1 + rel_index)
            rev_spectrum = [(float(intensity), float(mass)) for
                            mass, intensity in zip(spectrum[0], spectrum[1])]
            #
            new_qints = get_qint(rev_spectrum)
            qints = [a + b for a, b in zip(new_qints, qints)]
        
        #intensity reported ions (quantified)
        q_qints = get_quant(qints)
        qints = ['%.1f' % value for value in qints]
        #
        diccio[item].extend(qints)
        diccio[item].extend(q_qints)
    return diccio
#
#
#
if __name__ == '__main__':
    #
    spectra = [ [( 115.3, 114), ( 189.9, 115), (  77.1, 116), ( 110.4, 117),
                   ( 510.4, 126), ( 110.4, 127), (   5.3, 128), ( 189.9, 129)],
                  [(  57.4, 114), (  56.5, 115), (  65.9, 116), (  74.9, 117),
                   (   2.4, 126), (  10.4, 127), ( 115.3, 128), ( 100.9, 129)],
                  [( 113.9, 114), (1171.4, 115), (2253.5, 116), (3505.5, 117),
                   (  20.4, 126), (  11.4, 127), ( 115.3, 128), ( 189.0, 129)],
                  [(  10.0, 114), (   6.7, 115), (   9.0, 116), (  15.9, 117),
                   ( 175.4, 126), (  11.4, 127), (  11.3, 128), (1189.9, 129)]
                ]
                
    get_qint = get_qint_calculator(calidata, 'itraq')
    get_quant = get_quant_calculator(iso_itq)
                                
    print 'itraq'
    for spectrum in spectra:
        qints = get_qint(spectrum)
        q_qints = get_quant(qints)
        
        print ['%.1f' % value for value in qints]
        print q_qints
        print

    get_qint = get_qint_calculator(calidata, 'tmt')
    get_quant = get_quant_calculator(iso_tmt)
                                
    print 'tmt'
    for spectrum in spectra:
        qints = get_qint(spectrum)
        q_qints = get_quant(qints)
        
        print ['%.1f' % value for value in qints]
        print q_qints
        print

    raw_input("press enter to exit")
#
##itraq
##['115.3', '189.9', '77.1', '110.4']
##['111.7', '181.0', '61.7', '107.2']
##
##['57.4', '56.5', '65.9', '74.9']
##['56.4', '51.2', '59.8', '71.9']
##
##['113.9', '1171.4', '2253.5', '3505.5']
##['91.8', '1104.1', '2050.9', '3403.9']
##
##['10.0', '6.7', '9.0', '15.9']
##['9.9', '5.8', '8.0', '15.5']
##
##tmt
##['510.4', '110.4', '5.3', '189.9', '0.0', '0.0']
##['508.8', '78.6', '-7.9', '193.8', '-8.0', '-7.3']
##
##['2.4', '10.4', '115.3', '100.9', '0.0', '0.0']
##['2.3', '6.9', '111.2', '98.1', '-6.2', '-3.8']
##
##['20.4', '11.4', '115.3', '189.0', '0.0', '0.0']
##['20.3', '6.9', '107.7', '188.0', '-9.9', '-7.1']
##
##['175.4', '11.4', '11.3', '1189.9', '0.0', '0.0']
##['175.4', '1.4', '-35.7', '1213.7', '-49.4', '-45.5']
##
##enter para salir
##
##Another result for TMT (iTraq is the same) Feb 2012
#tmt
#['510.4', '110.4', '5.3', '189.9', '0.0', '0.0']
#['508.8', '78.6', '-8.2', '194.6', '-11.1', '-1.1']
#
#['2.4', '10.4', '115.3', '100.9', '0.0', '0.0']
#['2.3', '6.9', '110.9', '98.9', '-8.5', '0.9']
#
#['20.4', '11.4', '115.3', '189.0', '0.0', '0.0']
#['20.3', '6.9', '107.3', '189.2', '-13.7', '0.4']
#
#['175.4', '11.4', '11.3', '1189.9', '0.0', '0.0']
#['175.4', '1.5', '-37.9', '1219.6', '-69.7', '-6.9']
#
#enter para salir