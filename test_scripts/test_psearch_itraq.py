#para chequear psearch_itraq_07

from psearch_itraq_07 import *
from time import time
#
espectros = [ [( 115.3, 114),( 189.9, 115),(  77.1, 116), ( 110.4, 117),(5010.4, 118),( 210.4, 119)],
              [(  57.4, 114),(  56.5, 115),(  65.9, 116), (  74.9, 117),(   2.4, 118),(  10.4, 119)],
              [(1123.9, 114),(1771.4, 115),(2253.5, 116), (3505.5, 117),(  20.4, 118),(  11.4, 119)],
              [(  10.0, 114),(   6.7, 115),(   9.0, 116), (  15.9, 117),(  75.4, 118),(   1.4, 119)]
            ]
#
#
def old(fq, pr):
    set_global_itraq(calidata)
                                
    for spectro in espectros:
        itraq = get_itraq_ions(spectro)
        #
        if pr:
            q_itraq = fq(itraq)
            print ['%.1f' %(value) for value in itraq]
            print q_itraq
            print
        
        
def new(fq, pr, tipo='itraq'):
    set_global_traq(calidata, tipo)
                                
    for spectro in espectros:
        traq = get_traq_ions(spectro)
        #
        if pr:
            q_itraq = fq(traq)
            print ['%.1f' %(value) for value in traq]
            print q_itraq
            print   
    
    
def test_speed(n, old, new):
    quant = get_itraq_quant(iso_itq)
    #
    t0 = time()
    for i in xrange(n): old(quant,False)
    t1 = time()
    for i in xrange(n): new(quant, False)
    t2 = time()    
    #
    old = t1-t0
    new = t2-t1
    rat = (old-new) * 100 / old
    
    print "old ", old
    print "new ", new
    print "speed = %.1f" %rat
    print ""    
    

def test_equal(old, new):
    quant = get_itraq_quant(iso_itq)
    old(quant,True)
    new(quant, True)


if __name__ == "__main__":
    
    n = 20000
    test_speed(n, old, new)
    
    test_equal(old, new)
    
    quant = get_itraq_quant(iso_tmt)
    new(quant, True, 'tmt')
    
    
    raw_input("enter para salir")
    

###old  0.484000205994
##new  0.671999931335
##speed = -38.8
##
##['115.3', '189.9', '77.1', '110.4']
##['111.7', '181.0', '61.7', '107.2']
##
##['57.4', '56.5', '65.9', '74.9']
##['56.4', '51.2', '59.8', '71.9']
##
##['1123.9', '1771.4', '2253.5', '3505.5']
##['1091.1', '1642.2', '2016.6', '3405.0']
##
##['10.0', '6.7', '9.0', '15.9']
##['9.9', '5.8', '8.0', '15.5']
##
##['115.3', '189.9', '77.1', '110.4']
##['111.7', '181.0', '61.7', '107.2']
##
##['57.4', '56.5', '65.9', '74.9']
##['56.4', '51.2', '59.8', '71.9']
##
##['1123.9', '1771.4', '2253.5', '3505.5']
##['1091.1', '1642.2', '2016.6', '3405.0']
##
##['10.0', '6.7', '9.0', '15.9']
##['9.9', '5.8', '8.0', '15.5']
##
##['115.3', '189.9', '77.1', '110.4', '5010.4', '210.4']
##['111.6', '184.7', '-62.1', '-1940.8', '5137.8', '-20.1']
##
##['57.4', '56.5', '65.9', '74.9', '2.4', '10.4']
##['56.4', '51.1', '60.0', '74.4', '-7.2', '7.7']
##
##['1123.9', '1771.4', '2253.5', '3505.5', '20.4', '11.4']
##['1091.1', '1642.0', '2020.5', '3479.4', '-171.3', '-121.5']
##
##['10.0', '6.7', '9.0', '15.9', '75.4', '1.4']
##['9.9', '5.9', '6.1', '-15.3', '77.5', '-2.7']
##
##enter para salir

##incluyend fq(traq) la parte comun mas lenta)
##old  5.9690
##new  6.0939
##speed = -2.1
##enter para salir