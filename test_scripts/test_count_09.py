from time import time
import operator

n=1000
peptido = "-.ASDF$ASDF$M@ASFRM@AASDF$ASDF$M@ASFRM@AASDF$M@ASFRSFRASDF$M@ASFRSFR.T"

#
def cuenta():
    count = 0
    for item in peptido:
        if item.isalpha(): count += 1

    return count
    
#
def cuenta_2():
    
    return sum(1 for item in peptido if item.isalpha())

#
def cuenta_3():

    return reduce(operator.add, (1 for item in peptido if item.isalpha()))
    


t0 = time()
for i in range(n): uno=cuenta()
t1 = time()
for i in range(n): dos=cuenta_2()
t2 = time()
for i in range(n): tres=cuenta_3()
t3 = time()


print uno
print dos
print tres

print t1-t0
print t2-t1
print t3-t2


##    55
##    55
##    0.0150001049042
##    0.0159997940063
##
##    C:\Python25\programas\psearch_13>