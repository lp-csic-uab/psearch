from time import time

here = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]
m=1000

def uno():
    for n in xrange(m):
        for match in here:
            line = ["hola"]
            for tag in here:
                elementtext = (match, tag)
                line.append(elementtext)
            
    return line        
            


def dos():
    
    for n in xrange(m):
        for match in here:
            line = ["hola"] + [(match, tag) for tag in here]
    return line

    
    
    
if __name__ == "__main__":
    t0=time()
    a = uno()
    t1=time()
    b = dos()
    t2=time()
    
    print a
    print b
    print t1-t0
    print t2-t1
    
    
##['hola', (18, 1), (18, 2), (18, 3), (18, 4), (18, 5), (18, 6), (18, 7), (18, 8),
## (18, 9), (18, 10), (18, 11), (18, 12), (18, 13), (18, 14), (18, 15), (18, 16),
##(18, 17), (18, 18)]
##['hola', (18, 1), (18, 2), (18, 3), (18, 4), (18, 5), (18, 6), (18, 7), (18, 8),
## (18, 9), (18, 10), (18, 11), (18, 12), (18, 13), (18, 14), (18, 15), (18, 16),
##(18, 17), (18, 18)]
##0.125
##0.0780000686646
##
##C:\Python25\programas\psearch_13\test_scripts>