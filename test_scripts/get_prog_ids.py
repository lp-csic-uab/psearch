#Filename: getProgIDsFromWBEM.py
# Dispatch WBEM
#No lo pilla tdo. No pilla Xcalibur XRaw
from win32com.client import Dispatch
#
wbem = Dispatch('WbemScripting.SWbemLocator')
wbemServer = wbem.ConnectServer('.', 'root\cimv2')
# Extract progIDs
sql = 'SELECT ProgID FROM Win32_ProgIDSpecification'
results = wbemServer.ExecQuery(sql)
progIDs = [x.ProgID for x in results]
progIDs.sort()
# Record results
outputFile = open('progIDs_partialFromWBEM.txt', 'wt')
outputFile.write('\n'.join(progIDs))
outputFile.close()
