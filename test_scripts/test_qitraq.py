from numpy import matrix
from scipy.linalg import solve

# *** isodata modificado para test  ****
#           114   115    116    117
isodata = [[1.000, 0.563, 0.002, 0.000],    #R114
           [0.020, 1.000, 0.060, 0.001],    #R115
           [0.000, 0.030, 1.000, 0.049],    #R116
           [0.000, 0.000, 0.040, 1.000]     #R117
          ]

##isodata = [[1, 3, 5],
##            [2,5,1],
##            [2,3,8]
##           ]

A   = matrix(isodata)

def quant_itraq(A, intens):
    "es erroneo. solve ya transpone A"
    At = A.transpose()
    b = matrix(intens).transpose()
    matriz = solve(At,b).transpose()
    return ['%.1f'%(item) for item in matriz[0]]

def quant_itraq2(A, intens):
    ""
    b = matrix(intens).transpose()
    matriz = solve(A,b).transpose()
    #
    return ['%.1f'%(item) for item in matriz[0]]

if __name__ == '__main__':
    ""
    intensity = [57.4, 56.5, 65.9, 74.9]
    #intensity = [10, 8, 3]
    print ['%.1f'%(item) for item in intensity]
    print quant_itraq(A, intensity)  #old julio 2009 error..... No No estoesta bien!!!!
    print quant_itraq2(A, intensity) #corregido  ....Esto esta mal!!
    
###
## ['57.4', '56.5', '65.9', '74.9']
## ['56.9', '22.6', '61.6', '71.9']
## ['27.9', '52.2', '60.8', '72.5']
## Script terminated.
###
##    We could find the solution vector using a matrix inverse
##    However, it is better to use the linalg.solve command which can
##    be faster and more numerically stable. In this case it however gives
##    the same answer as shown in the following example:
##                                                     
##    >>> A = mat('[1 3 5; 2 5 1; 2 3 8]')
##    >>> b = mat('[10;8;3]')
##    >>> A.I*b
##    matrix([[-9.28],
##            [ 5.16],
##            [ 0.76]])
##    >>> linalg.solve(A,b)
##    array([[-9.28],
##           [ 5.16],
##           [ 0.76]])
