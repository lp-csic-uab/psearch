from matplotlib import pyplot
pyplot.interactive(True)
import random
import numpy
from time import time

size = 100000
ri = random.randint
rsize = [20]*size
color1 = ['r'] * size
color2 = ['b'] * size

a = [[ri(0,20),ri(0,20),ri(0,20)] for item in range(size)]


t0 =time()

#*********************************
#
xx=[]
yy=[]
zz=[]

xa = xx.append
ya = yy.append
za = zz.append

[(xa(x), ya(y), za(z)) for x,y,z in a]
   
t1 = time() 
pyplot.plot(xx,yy, 'ro')

t2 = time()
print t1-t0
print t2-t1
# 0.13
# 4.51   #el problema esta en el scatter. plot es x5 veces mas rapido


#*********************************
#
##xx=[]
##yy=[]
##zz=[]
##
##xa = xx.append
##ya = yy.append
##za = zz.append
##
##[(xa(x), ya(y), za(z)) for x,y,z in a]
##   
##t1 = time() 
##pyplot.scatter(xx,yy, rsize, color1)
##
##t2 = time()
##print t1-t0
##print t2-t1
### 0.11
###27.92   #el problema esta en el scatter

#*******************************
#
##xx=[]
##yy=[]
##zz=[]
##
##xa = xx.append
##ya = yy.append
##za = zz.append
##
##for x,y,z in a:
##    xa(x)
##    ya(y)
##    za(z)
##    
##pyplot.scatter(xx,yy, rsize, color1)
##
##t1 = time()
##print t1-t0
###28.0

#*************************************
#
##xx=[]
##yy=[]
##zz=[]
##for x,y,z in a:
##    xx.append(x)
##    yy.append(y)
##    zz.append(z)
##    
##pyplot.scatter(xx,yy, rsize, color1)
##
##t1 = time()
##print t1-t0
###28.11

#*****************************************
#
##r = numpy.array(a)
##[x2,y2,z2] = r.swapaxes(0,1)
##
##pyplot.scatter(x2,y2, rsize, color2)
##
##t1 = time()
##print t1-t0
###28.43

raw_input()