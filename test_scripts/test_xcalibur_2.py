from matplotlib import pyplot
import time

#makepy for XDK XcCalibur Files 1.2
import win32com.client as wc
xr = wc.Dispatch('XRaw.Xraw.1')
xr.Open("C:\\test.raw")
dr = xr.Detector(0,1)
#dr
#<win32com.gen_py.XDK Xcalibur Files 1.2 Type Library.IXDetectorRead2 instance at 0x22361112>
#dr.AcquisitionDate
#u'2/28/96'
#dr.EndTime
#3.79
sp=dr.Spectra(0)
#sp
#<win32com.gen_py.XDK Xcalibur Files 1.2 Type Library.IXSpectra instance at 0x24820912>
#sp.Count
#325
#
filters = dr.Filters
#
scan = sp.Item(12)
#scan.Header
#<win32com.gen_py.XDK Xcalibur Files 1.2 Type Library.IXSpectrumHeader instance at 0x24821272>

xx=[]
fll = []
rtl = []

for spectrum in range(1,200):
    scan = sp.Item(spectrum)
    rt = sp.IndexToRetentionTime(spectrum,None,None)
    fl = filters.ScanNumber(spectrum).Text
    
    xx.append(scan.Data[0:2])
    fll.append(fl)
    rtl.append(rt[1])
    
    #print x
    
pyplot.interactive(True)
pyplot.hold(False)
ax= pyplot.axes()
tax = ax.transAxes
##pyplot.show()

minx=100
maxx=500
for idx, item in enumerate(xx):
    xmin = min(item[0])
    xmax = max(item[0])
    if xmin < minx : minx=xmin
    if xmax > maxx : maxx=xmax
    x = [minx-1] + [xitem for xitem in item[0]] + [maxx+1]
    y = [1] + [yitem for yitem in item[1]] + [1]
    
    txt = 'Scan = %i\nRet time = %.4f\n%s' %(idx, rtl[idx], fll[idx])
    pyplot.bar(x, y, width=0.5)
    pyplot.text(0.2,0.88,txt, transform=tax)
    #pyplot.show()
    #time.sleep(0.2)
    #raw_input()


##text = ""
##n=0
##for tup in xx:
##    n += 1
##    text += 'File = '+ str(n) + '\n'
##    for item in tup:
##        item = [str(i) for i in item]
##        text += ', '.join(item)+'\n\n'
##    
##open("C:\\hola2.txt",'w').write(text)
#
#scan.Data
#((165.72113037109375, 166.6666259765625, 188.2767333984375, 189.19998168945312, 287.81927490234375, 288.7332763671875, 307.2830810546875, 308.199951171875, 310.4666748046875, 312.5999755859375, 314.7332763671875, 316.86663818359375,
# 319.0, 321.13330078125, 323.2666015625, 325.39996337890625, 327.5333251953125, 329.6666259765625, 331.79998779296875, 333.933349609375, 336.066650390625, 338.199951171875, 340.33331298828125, 342.4666748046875, 344.5999755859375,
# 346.7332763671875, 348.86663818359375, 351.0, 353.13330078125, 355.2666015625, 357.39996337890625, 358.72113037109375, 359.6666259765625),
# (307.0, 0.0, 495.0, 0.0, 535.0, 0.0, 2485.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 307.0, 0.0),
# (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
# (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
# (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
# (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
# (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
# (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
# (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
# (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0))
