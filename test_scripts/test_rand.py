from numpy.random import rand
from time import time

matrix_1 = rand(100000,3)
matrix_2 = rand(100000,3)

def comp():
    fkeys = []
    fpbx = []
    fpby = []
    
    for lista in [matrix_1, matrix_2]:
        pbx = [x for k, x, d in lista]
        pby = [d for k, x, d in lista]
        keys = [k for k, x, d in lista]
        fkeys.extend(keys)
        fpbx.extend(pbx)
        fpby.extend(pby)

def clasic():
    fkeys = []
    fpbx = []
    fpby = []
    for lista in [matrix_1, matrix_2]:
        for k, x, d in lista:
            fpbx.append(x)
            fpby.append(d)
            fkeys.append(k)

if __name__ == "__main__":
    t0 =time()
    comp()
    t1 =time()
    clasic()
    t2 =time()
    print t1-t0
    print t2-t1
    
##1.5150001049
##0.68799996376
##
##C:\Python25\programas\psearch_11>