from time import time
import operator

new = map(float, xrange(0,100,2))
old = map(float, xrange(1,101,2))

#print new, old
n = 10000


t0 = time()

for i in range(n): qints = [a + b for a, b in zip(new, old)]

t1 = time()

for i in range(n): qints2 = map(operator.add, new,old)

t2 = time()

print qints
print qints2

print t1-t0
print t2-t1

##    [1.0, 5.0, 9.0, 13.0, 17.0, 21.0, 25.0, 29.0, 33.0, 37.0, 41.0, 45.0, 49.0, 53.0
##    , 57.0, 61.0, 65.0, 69.0, 73.0, 77.0, 81.0, 85.0, 89.0, 93.0, 97.0, 101.0, 105.0
##    , 109.0, 113.0, 117.0, 121.0, 125.0, 129.0, 133.0, 137.0, 141.0, 145.0, 149.0, 1
##    53.0, 157.0, 161.0, 165.0, 169.0, 173.0, 177.0, 181.0, 185.0, 189.0, 193.0, 197.
##    0]
##    [1.0, 5.0, 9.0, 13.0, 17.0, 21.0, 25.0, 29.0, 33.0, 37.0, 41.0, 45.0, 49.0, 53.0
##    , 57.0, 61.0, 65.0, 69.0, 73.0, 77.0, 81.0, 85.0, 89.0, 93.0, 97.0, 101.0, 105.0
##    , 109.0, 113.0, 117.0, 121.0, 125.0, 129.0, 133.0, 137.0, 141.0, 145.0, 149.0, 1
##    53.0, 157.0, 161.0, 165.0, 169.0, 173.0, 177.0, 181.0, 185.0, 189.0, 193.0, 197.
##    0]
##    0.219000101089
##    0.077999830246
##
##    C:\Python25\programas\psearch_13>