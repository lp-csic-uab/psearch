"""
sub extraer_ms{
my $archivo_raw=$_[0];
my %correspondencias;
use Win32::OLE;
use Win32::OLE::Variant;


my $enlaceRaw =Win32::OLE->new('XRawfile.XRawfile.1', sub{ $_[0]->Close } );
$enlaceRaw->Open($archivo_raw);
$enlaceRaw->SetCurrentController( 0, 1 );


my $first = Win32::OLE::Variant->new(VT_I4|VT_BYREF,0);
	my $last = Win32::OLE::Variant->new(VT_I4|VT_BYREF,0);
	$enlaceRaw->GetFirstSpectrumNumber($first);
	$enlaceRaw->GetLastSpectrumNumber($last);
my $cuenta_scans=1;
	for my $i ( $first .. $last ){
		# do not pass an initialization value to new or it won't work
		# this requires Activestate perl 5.8.*
		my $filter = Win32::OLE::Variant->new(VT_BSTR|VT_BYREF);
		$enlaceRaw->GetFilterForScanNum($i,$filter);
		if ($filter=~ /\sms3\s/i){
			$correspondencias{$cuenta_scans}=3;
			}
		elsif($filter=~ /\sms2\s/i){
			$correspondencias{$cuenta_scans}=2;
			}
		$cuenta_scans++;
		
	}
return \%correspondencias;
"""

from win32com.client import Dispatch, gencache, constants
#gencache.EnsureModule('{5FE970A2-29C3-11D3-811D-00104B304896}', 0, 1, 0)

xcl= Dispatch("XRawfile.XRawfile.1")
xcl.Open("C:\\test.raw")
xcl.SetCurrentController(0,1)
#
myfirst = xcl.GetFirstSpectrumNumber(None)
mylast = xcl.GetLastSpectrumNumber(None)

for idx in range(myfirst, mylast):
    filter = xcl.GetFilterForScanNum(idx,None)
    print filter
