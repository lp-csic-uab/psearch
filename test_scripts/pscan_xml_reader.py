import os
import xml.etree.ElementTree as et
from p_search_01.psearch_raw_class_01 import RawExtractor

filename = 'test.xml'
DIR = "C:"

tags = ['reference', 'peptide', 'scan_range', 'charge',
        'search_time', 'intensity', 'actual_mass', 'input_mass',
        'xcorr', 'deltacn', 'sp', 'rsp', 'ions', 'count']
#
header = "\t".join(['File'] + tags)
#
doc = et.parse(os.paht.join(DIR, filename))
matches = doc.findall("peptide_match")

string = type('str')
data = [header]
for match in matches:
    line = [filename]
    for tag in tags:
        element = match.find(tag)
        try:
            elementtext = element.text
            assert(type(elementtext) is string)
        except (AttributeError, AssertionError):
            elementtext = ""
        line.append(elementtext)
    linetxt = "\t".join(line)
    data.append(linetxt)
    
fulltxt = "\n".join(data)
open("C:/outtext.xls", 'w').write(fulltxt)