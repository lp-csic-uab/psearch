
---

**WARNING!**: This is the *Old* source-code repository for PSearch program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/psearch/) located at https://sourceforge.net/p/lp-csic-uab/psearch/**  

---  
  

![https://lh6.googleusercontent.com/-k2xCefGutBI/T1EJQLOEHVI/AAAAAAAAATE/B2O9kmWhAF8/s800/Psearch.gif](https://lh6.googleusercontent.com/-k2xCefGutBI/T1EJQLOEHVI/AAAAAAAAATE/B2O9kmWhAF8/s800/Psearch.gif)


---

**WARNING!**: This is the *Old* source-code repository for PSearch program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/psearch/) located at https://sourceforge.net/p/lp-csic-uab/psearch/**  

---  
  

**Table Of Contents:**

[TOC]

#### Description

**`PSearch`** is a tool for processing shotgun data. Psearch extracts MS data deriving from an MDLC ESI-ITMS3 analysis and prepares it to be loaded on a LymPHOS 1.0 Phos-file.

Data is extracted from a Thermo Bioworks report exported to XML and from the original .RAW files.

![https://lh6.googleusercontent.com/-nlKSkvvhK3Q/T1EJQFp43EI/AAAAAAAAATI/Knnr-mHsNSc/s400/psearch160_main.png](https://lh6.googleusercontent.com/-nlKSkvvhK3Q/T1EJQFp43EI/AAAAAAAAATI/Knnr-mHsNSc/s400/psearch160_main.png)

#### Features

  * Reports
    * tsv (.xls) report with analytical and identification data from all Bioworks identifications:
      * 'File', 'scan', 'ms\_level', 'reference', 'actual\_mass', 'dmass', 'charge', 'peptide', 'Prob', 'xcorr', 'deltacn', 'sp', 'rsp', 'ions', 'count', 'D'
    * Final tsv report containing fdr filtered identifications and optionally:
      * raw and corrected quantitative data from reporter ions
      * coincident ms2-ms3 identifications
  * Can analyze different combinations of MS/MS scan cycles, including:
    * n pqdMS2 + 1 cidMS2 + 1 cidMS3   (p.e. phosphopeptides with itraq, n = 3)
    * 1 cidMS2 + 1 cidMS3              (p.e. phosphopeptides qualitative)
    * n pqdMS2                         (p.e. general quant analysis, n = 1)
  * Quantitation
    * iTRAQ and TMT data analysis
    * User calibration of reporter mass and mass window
    * table to set bacht specific isotope contributions to each reporter signal
    * reporter quantitation with correction of isotopic contribution
  * FDR filtering
    * 2-dimensional FDR setting using D-value and X-Correlation
    * Tool to characterize the id of filtered or unfiltered target and decoy identifications
    * Tool to evaluate better FDR boundary settings.
    * Automatic iterative process that selects best FDR line parameters.
  * Tool for checking and fixing Thermo Bioworks malformed XML files

![https://lh6.googleusercontent.com/-BS57Luth3Ec/T1EJQGoU3NI/AAAAAAAAATM/KYW-eyYtF6k/s640/psearch_views.png](https://lh6.googleusercontent.com/-BS57Luth3Ec/T1EJQGoU3NI/AAAAAAAAATM/KYW-eyYtF6k/s640/psearch_views.png)

#### Installation

Tested in Windows XP 32 bits and Windows 7 64 bits.

##### Requirements

  * Thermo Bioworks Libraries: `PSearch` uses the Thermo Bioworks XRaw and XRawfile libraries, so Bioworks has to be installed on your computer.

##### From Installer

  1. Download `PSearch_x.y_setup.exe` windows installer from repository at the [PSearch project download page](https://bitbucket.org/lp-csic-uab/psearch/downloads).
  1. [Get your Password](#markdown-header-download) for the installer.
  1. Double click on the installer and follow the Setup Wizard.
  1. Run `PSearch` by double-clicking on the `PSearch` short-cut in your desktop or from the START-PROGRAMS application folder created by the installer.

##### From Source

  1. Install Python and third party software indicated in [Dependencies](#markdown-header-source-dependencies).
  1. Download `PSearch` source code from its [Mercurial Source Repository](https://bitbucket.org/lp-csic-uab/psearch/src).
  1. Download commons source code from its [Mercurial Source Repository](https://bitbucket.org/lp-csic-uab/commons/src).
  1. Copy the folders in python site-packages or in another folder in the python path.
  1. Run `PSearch` by double-clicking on the `psrch_main.pyw` module.

###### _Source Dependencies:_

  * [Python](http://www.python.org/) 2.6 or 2.7 (not tested with other versions)
  * [wxPython](http://www.wxpython.org/) 2.8.11.0 - 2.8.11.2
  * [matplotlib](http://matplotlib.sourceforge.net/)
  * [scipy](http://www.scipy.org/)
  * [psyco](http://psyco.sourceforge.net/)
  * [commons](https://bitbucket.org/lp-csic-uab/commons/src) (from LP CSIC/UAB BitBucket [repository](https://bitbucket.org/lp-csic-uab/commons/src))

Third-party program versions correspond to those used for the installer available here. Lower versions have not been tested, although they may also be fine.

##### Post-Install

To Do!

#### Download

![https://lh6.googleusercontent.com/-LQE2us7J9GI/TnMstHYmquI/AAAAAAAAAKU/HgdPvan2S08/s800/downloadicon.jpg](https://lh6.googleusercontent.com/-LQE2us7J9GI/TnMstHYmquI/AAAAAAAAAKU/HgdPvan2S08/s800/downloadicon.jpg) You can download the last version of **`PSearch`** [here](https://bitbucket.org/lp-csic-uab/psearch/downloads).

After downloading the binary installer, you have to e-mail us at ![https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png](https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png) to get your free password and unlock the installation program. The password is not required to run the application from source code

#### Change-Log

1.6dev February 2012

  * English translations
  * Extensive refactoring
  * relocation of scripts not related with the package
  * moving several modules to commons (warns, iconic, maniclock)
  * version number on module names eliminated

1.5 January 2010

  * 

1.4 November 2009

  * 

1.3 November 2009

  * 

1.0 September 2009

  * 

0.6 June 2009

  * 

#### To-Do

  * Hacer que la file con datos originales se salve el mismo nombre que el log (basado en el tiempo de ordenador). Ahora su nombre es siempre 'full\_outfile' -> no
  * Mejorar el funcionamiento del corte con circulo
  * Documentar algunas funciones
  * Poner iconos en las ventanas de herramientas ? -> no
  * Crear un aviso que desaparezca soloCreate a user-modifiable configuration file to hold directory and filelocation currently hard-coded in source file _kblast\_common.py_
  * Interface refinements

#### Contribute

These programs are made to be useful. If you use them and don't work entirely as you expect, let us know about features you think are needed, and we will try to include them in future releases.

![https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png](https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png)


---

**WARNING!**: This is the *Old* source-code repository for PSearch program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/psearch/) located at https://sourceforge.net/p/lp-csic-uab/psearch/**  

---  
  
