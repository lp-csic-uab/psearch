# exWx/setup.py
#
## python setup.py py2exe 
#
from distutils.core import setup
import os, sys
import ConfigParser
import matplotlib
#noinspection PyUnresolvedReferences
import py2exe
#
sys.argv.append('py2exe')
config = ConfigParser.ConfigParser()
config.readfp(open('install.ini'))
#
PyName = config.get('Setup', 'pyname')
AppVersion = config.get('Common', 'version')
AppName = config.get('Common', 'name')
Icondir = config.get('Common', 'icondir')
Icon = config.get('Common', 'big_icon')
#
Icon = os.path.join(Icondir, Icon)
#
def get_docs():
    folder = 'doc'
    docs = []
    for arch in os.listdir(folder):
        docs.append(os.path.join(folder, arch))
    return [("doc", docs)]
#
#
####################################
#
#      psrch_main.pyw
#
####################################
#
#
setup(
    windows = [
            {'script': PyName,
             'icon_resources':[(0, Icon)],
             'dest_base' : AppName,
             'version' : AppVersion,
             'company_name' : "JoaquinAbian",
             'copyright' : "No Copyrights",
             'name' : AppName
        }
    ],
            
    options = {
              'py2exe': {
                        'packages' :    ['matplotlib', 'pytz'],
##                        'includes':     [],
                        'excludes':     ['EasyDialogs', 'PyQt4',
                                         'bsddb', 'curses', 'Pyrex',
                                         'email', 'testing', 'fft',
                                         '_gtkagg', '_tkagg', 'pywin.debugger',
                                         'pywin.debugger.dbgcon', 'pywin.dialogs',
                                         'Tkconstants','Tkinter', 'tcl'
                                        ],
                        'ignores':       ['wxmsw26uh_vc.dll'],
                        'dll_excludes': ['libgdk_pixbuf-2.0-0.dll',
                                         'libgdk-win32-2.0-0.dll',
                                         'libgobject-2.0-0.dll',
                                         'tk84.dll','tcl84.dll'
                                        ],
                        'compressed': 1,
                        'optimize':2,
                        'bundle_files': 1
                        }
              },
    zipfile = None,
    #Additional files I want in folder  'dist'.
    #The packer (inno) can go to the main dir to get them but then
    #they would not be in 'dist' and the program.exe there would not work.
    data_files = get_docs() + \
                 [("", ["INSTALL.txt",   #Installation final page
                        "README.txt",    #README and help (English)
                        "README.ESP.txt",   #README and help (Espanyol)
                        "README.CAT.txt",   #README and help (Catala)
                        "LICENCE.TXT",  #Licence File (GPL v3)
                        "msvcp90.dll",
                        "C:\\Python26/Lib/site-packages/wx-2.8-msw-unicode/wx/gdiplus.dll"
                       ]
                 )] + \
                 [('', ['C:\\Python26\\programas\\_psearch\\psearch_config'],)] + \
                 matplotlib.get_py2exe_datafiles()
    )
