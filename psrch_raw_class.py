#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
psearch_raw_class (psearch)
15 june 2009
#
Classes that extracts ms data, tr, scan, ms level, etc... from each
Thermo .raw file.
#
These classes crash when extracting big .raw files.
A 219 Mb raw file can still be analyzed if run alone, p.e.:

"test_com.py"
    import time
    from psearch_raw_class_01 import RawExtractor
    rex = RawExtractor('C:/MONTSE/raw/Lymphos_iTRAQ_1mg_IMAC_Fr46.RAW')
    print "finish"
    time.sleep(20)
    for i in range(2,20):
        level = rex.get_ms_level(i)
        print i, level
        time.sleep(10)

but when working in batch mode, script_psearch.py crashes at this file with
"Fatal Python error: PyEval_RestoreThread: NULL tstate"
Elimination of psyco frees some more virtual memory and deleting each of the
created com objects seems also to help. In these conditions it works again but
probably in critical conditions.

The memory data (Mb):
                               ------ script_psearch ------
        initial  +RAM_virtual  part_1_ms2_ms3   part_2_quant
total    512        512             2284         2284      (1,5 Gb increment!)
lim     2205       2992             2992         2992
max     2266       2377             2377         2377

Edit: It seems that letting it sleep between files also helps

"""
#
import win32com.client as wc
import pywintypes
import time
#
#
#noinspection PyUnresolvedReferences
class RawExtractor():
    """Extractor with Xraw COM object
    
    Xraw.Xraw allows spectra reading but requires to load the full file thus
    giving memory problems. Moreover, it produces many errors when reading the
    spectrum filter.
    """
    def __init__(self, rawfile, full=False):
        """Constructor. Opens the Xraw object

        Uses a while loop to retry after a failure (p.e. produced by a big file)

        """
        n = 0
        while n < 3:
            xraw = None
            try:
                xraw = wc.Dispatch('XRaw.Xraw.1')
                xraw.Open(rawfile)
                self.success = True
                break
            except pywintypes.com_error:
                print 'error XRaw'
                del xraw
                time.sleep(5)
                n += 1
        #if it doesn't go out through break
        else:
            self.success = False
            return
        #
        xraw = xraw.Detector(0, 1)
        self.filters = xraw.Filters
        
        if full:
            self.xsp = xraw.Spectra(0)
        else:
            self.xsp = None
    #
    #
    def get_rt(self, item):
        """(scan, rt) <--> (0, 0.0046666666666666671)"""
        try:
            return self.xsp.IndexToRetentionTime(item, None, None)
        except AttributeError:
            return 'err'
    #
    def get_filter(self, item):
        """+ c Full ms2 363.30@cid40.00 [150.00-375.00]"""
        try:
            return self.filters.ScanNumber(item).Text
        except (AttributeError, pywintypes.com_error):
            return "err1"
    #
    def get_ms_level_spectra(self, item):
        """Read ms level from spectrum data or (if that fails) from filter"""
        try:
            scan = self.xsp.Item(item)
            xsp = scan.ParentScans        #not in Thermo docs
            return 'ms%i' %(xsp.Count + 1,)
        except (AttributeError, pywintypes.com_error):
            self.get_ms_level(item)
        
    def get_ms_level(self, item):
        """Extracts ms level from spectrum filter"""
        text = self.get_filter(item)
        print text
        index = text.find('ms')
        if index > -1:
            level = text[index:index+3]
            return level
        else:
            return "err2"
    #
    def get_ms(self, item):
        """"""
        try:
            scan = self.xsp.Item(item)
            return scan.Data[0:2]
        except pywintypes.com_error:
            #print 'error', item
            return [(0,),(0,)]
    #
    def get_parent(self, item):
        """"""
        #print item
        try:
            scan = self.xsp.Item(item)
            xps = scan.ParentScans  #not in thermo docs
        except (AttributeError, pywintypes.com_error):
            return "-1"

        return xps.Item(1).ScanNumber, xps.Item(1).MonoIsoMass
    #
    def close(self):
        """"""
        if self.xsp: self.xsp.Close()
#
#
#noinspection PyUnresolvedReferences
class RawFileExtractor():
    """Extractor with Xrawfile COM object.
    
    XRawfile.XRawfile access raw file data without loading it in full. However
    it doesnt allow to get the spectrum.

    """
    def __init__(self, rawfile):
        """Constructor. Opens the Xraw object

        Uses a while loop to retry after a failure (p.e. produced by a big file)

        """
        n = 0
        while n < 3:
            xraw = None
            try:
                xraw = wc.Dispatch('XRawFile.XrawFile.1')
                xraw.Open(rawfile)
                self.success = True
                break
            except pywintypes.com_error:
                print 'error XRawFile'
                del xraw
                time.sleep(5)
                n += 1
        #if it doesn't go out through break
        else:
            self.success = False
            return
        #
        self.xraw = xraw
        self.xraw.SetCurrentController(0,1)
        print 'Initiated XRawFile'
    #
    def get_rt(self, item):
        """(scan, rt) <--> (0, 0.0046666666666666671)"""
        return self.xraw.RTFromScanNum(item, None)
    #
    def get_filter(self, item):
        """+ c Full ms2 363.30@cid40.00 [150.00-375.00]"""
        try:
            return self.xraw.GetFilterForScanNum(item, None)
        except (AttributeError, pywintypes.com_error):
            return "err1"
    #
    def get_ms_level(self, item):
        """Extracts ms level from filter"""
        text = self.get_filter(item)
        index = text.find('ms')
        if index > -1:
            level = text[index:index+3]
            return level
        else:
            return "err2"
    #
    def close(self):
        """"""
        if self.xraw: self.xraw.Close()
#
#
#noinspection PyUnusedLocal
class MockExtractor():
    """"""
    def __init__(self, file=None):
        """Constructor. Fakes Extractor that always success"""
        self.success = 1
    #
    def get_rt(self, item):
        """"""
        return 0
    #
    def get_filter(self, item):
        """"""
        return "mock"
    #
    def get_ms_level(self, item):
        """"""
        return "mock"
    #
    def get_ms(self, item):
        """"""
        return [(0,),(0,)]
    #
    def get_parent(self, item):
        """"""
        return 0
    #
    def close(self):
        """"""
        pass
#
#
#
if __name__ == '__main__':
    
    test_file = "C:/python26/programas/psearch_14/test/test.raw"  #only MS2
    test_file = "C:/python26/programas/psearch_14/test/test_2.raw"
    raw_ex = RawExtractor(test_file)
    #print 'raw', raw_ex
    if raw_ex.success:
        for item in range(1,50):
        #for item in range(1,15):
            print "spectrum <%s>" % str(item)
            print raw_ex.get_filter(item)
            print "ms_level: <%s>" % str(raw_ex.get_ms_level(item))
            print "Rt --> <%s>" % str(raw_ex.get_rt(item))
            print "precursor <%s>" % str(raw_ex.get_parent(item))
    else:
        print 'Failure !!'
        
    raw_input()
##
##    espectro 1
##    + c Full ms2 363.30@cid40.00 [150.00-375.00]
##    Rt --> (0, 0.0046666666666666671)
##    precursor -1
##    espectro 2
##    + c Full ms2 363.30@cid40.00 [150.00-375.00]
##    Rt --> (1, 0.016333333333333332)
##    precursor -1
##    espectro 3
##    + c Full ms2 363.30@cid40.00 [150.00-375.00]
##    Rt --> (2, 0.028166666666666666)
##    precursor -1
##    espectro 4
##    + c Full ms2 363.30@cid40.00 [150.00-375.00]
##    Rt --> (3, 0.03966666666666667)
##    precursor -1
##    espectro 5
##    + c Full ms2 363.30@cid40.00 [150.00-375.00]
##    Rt --> (4, 0.051499999999999997)
##    precursor -1
##    espectro 6
##    + c Full ms2 363.30@cid40.00 [150.00-375.00]
##    Rt --> (5, 0.063)
##    precursor -1
##    espectro 7
##    + c Full ms2 363.30@cid40.00 [150.00-375.00]
##    Rt --> (6, 0.074666666666666673)
##    precursor -1
##    espectro 8
##    + c Full ms2 363.30@cid40.00 [150.00-375.00]
##    Rt --> (7, 0.086499999999999994)
##    precursor -1
##    espectro 9
##    + c Full ms2 363.30@cid40.00 [150.00-375.00]
##    Rt --> (8, 0.098166666666666666)
##    precursor -1
##    espectro 10
##    + c Full ms2 363.30@cid40.00 [150.00-375.00]
##    Rt --> (9, 0.10983333333333334)
##    precursor -1
##    espectro 11
##    + c Full ms2 363.30@cid40.00 [150.00-375.00]
##    Rt --> (10, 0.12133333333333333)
##    precursor -1
##    espectro 12
##    + c Full ms2 363.30@cid40.00 [150.00-375.00]
##    Rt --> (11, 0.13300000000000001)
##    precursor -1
##    espectro 13
##    + c Full ms2 363.30@cid40.00 [150.00-375.00]
##    Rt --> (12, 0.14466666666666667)
##    precursor -1
##    espectro 14
##    + c Full ms2 363.30@cid40.00 [150.00-375.00]
##    Rt --> (13, 0.15633333333333332)
##    precursor -1
##
##
##    C:\Python25\programas\psearch_08>