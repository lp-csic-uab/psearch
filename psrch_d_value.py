#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
psrch_d_value (psearch)
15 de junio de 2009
"""
#
import math
#
#
def get_dvalue(xc, dcn, rsp, dmass, lg):
    """Calculates d_value

    xc -> XCorr (SEQUEST CrossCorrelation)
    dcn -> DCn
    lg -> peptide length

    """
    log = math.log
    #
    try:
        return (
                (8.4 * log(xc) / log(lg)) +
                (7.4 * dcn) - (0.2 * log(rsp)) -
                (0.3 * math.fabs(dmass)) - 0.96
                )
    except (ValueError, OverflowError):
        return -1
#
#
def count_aas(peptide):
    """Counts total number of amino acids in sequence

    Sequences are of the type '-.ASDF$M@ASFR.T'

    """
    try:
        peptide = peptide.split('.')[1]
    except (IndexError, AttributeError):
        return -1
    #
    count = 0
    for item in peptide:
        if item.isalpha(): count += 1
    #
    return count
#
#
#
if __name__ == '__main__':
    
    dmass = -1.17912
    peptide = "S.SMHYA.N"
    xc = 0.318
    dcn = 0.032
    rsp = 5
    D = -7.38
 
    dmass = -0.68
    peptide = "R.NQGGYGGSSSSSSYGS#GR.R"  #lg = 18 
    xc = 4.804
    dcn = 0.096
    rsp = 1
    D = 4.023779524                     #d = 4.10753
    
    lg = count_aas(peptide)
    print lg
    print get_dvalue(xc, dcn, rsp, dmass, lg)   