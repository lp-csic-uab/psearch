#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
psrch_txt (psearch)
10 june de 2009
"""
#

INTRO = u"""                                                       PSEARCH 1.6
                                             Process  Shotgun data
     
Extracts info derived from MDLC ESI-ITMS3 analyses and arrange it to be loaded in a LymPHOS Phos-file.
Info is extracted from a Bioworks result file exported to XML and the original RAW files.

The program can read data from any of the following scan cycles:

        3PQD-1CID-MS3  analysis itraq phosphopeptides
        1CID-MS3       analysis normal phosphopeptides
        1PQD           analysis cenit (needs testing)


The program generates 3 output files:
    
1) Document .xls with all data extracted from archive XML and the RAW file (optional).
   Contains:
   
  'File', 'scan','ms_level', 'reference', 'actual_mass', 'dmass', 'charge', --->
  ---> 'peptide', 'Prob', 'xcorr', 'deltacn', 'sp', 'rsp', 'ions', 'count''D'

2) Document .xls with filtered data plus coincident m2_ms3 (optional) and quantitation data (optional)
  
3) Log file with data relative to the search


PROBLEMS:

1) Loading RAW files with COM is memory expensive.
   A 200 Mb file takes more than 1 Gb of memory.
   Files of this size can crash the program in computers with limited RAM < 2 Gb.
   The use of these COM libraries limits scalability and requires Thermo XDK.
   No answer so far:
   - Check if mzXML files have all the info needed:
        - ms_level (?)
        - spectrum (yes)
        - tr       (?, I think it is needed for phos-file)

JAbian September 2009
"""
#
CABECERA="""\
MASS CALIBRATION REPORTER IONS
      (mmu)
    
Reporter Ion Reference Masses
    itraq-114     tmt-126
      114.2        126.2

  default settings
mass correction  -->  0 mmu
precision window -->450 mmu
"""

HEAD_SMODE=u"""\
              Typical Scan Modes
 n pqdMS2 + 1 cidMS2 + 1 cidMS3   (1)
                     1 cidMS2 + 1 cidMS3   (2)
 n pqdMS2                                        (3)

 1) phosphopeptides with itraq, n = 3
 2) phosphopeptides qualitative
 3) general analysis, n=1
"""

XML_TOOLS=u"""- check_xml
Detects malformed xml files in a directory.
You can use mod_xml to fix all of them automatically if the problem is the one described below.
In other cases you should correct them one by one before processing them with psearch.

- mod_xml
Corrects the lack of a '>' tag missed on Bioworks XML files.
"""