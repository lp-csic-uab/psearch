#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
scripts.script_check_mod_xml (psearch)
- check_xml
Detects malformed xml files in a directory.
You can use mod_xml to fix all of them automatically if the problem is the one described below.
In other cases you should correct them one by one before processing them with psearch.
- mod_xml
Corrects the lack of a '>' tag missed on Bioworks XML files.

1 de septiembre de 2009
"""
#
#
import os, wx
import traceback
import xml.etree.ElementTree as et
from xml.parsers.expat import ExpatError
#noinspection PyUnresolvedReferences
from wx._core import PyNoAppError
from cStringIO import StringIO
from psrch_class_xml_tools import XmlToolsFrame
from psrch_txt import XML_TOOLS
from commons.warns import tell
#
#
error_fatal = (KeyboardInterrupt, MemoryError)
#
#
#noinspection PyUnusedLocal
class XmlTools(XmlToolsFrame):
    def __init__(self, parent, *args, **kargs):
        XmlToolsFrame.__init__(self, parent, *args, **kargs)
        self.what = 1
        self.lb_tools.SetLabel(XML_TOOLS)
        title = "XML Tools"
        self.SetTitle(title)
        self.Bind(wx.EVT_BUTTON, self.on_bt_check, self.bt_chk_xml)
        self.Bind(wx.EVT_BUTTON, self.on_bt_fix, self.bt_fix_xml)
        self.Bind(wx.EVT_BUTTON, self.on_bt_quit, self.bt_quit)
    #
    def on_bt_check(self, evt):
        pathname = wx.DirSelector()
        if pathname:
            archive = "error.log.txt"
            error = []
            more = True
            try:
                for filepath in get_next_xml(pathname):
                    err, more = check_xml(filepath, more)
                    error.append(err)

                open(archive, 'w').write("\n".join(error))
                os.startfile(archive)
            except Exception:
                f = StringIO()
                traceback.print_exc(file=f)
                valor = f.getvalue()
                tell('Error NO completed \n\n%s' % valor)
    #
    def on_bt_fix(self, evt):
        pathname = wx.DirSelector()
        if pathname:
            try:
                for filepath in get_next_xml(pathname):
                    print pathname
                    mod_xml(filepath)
            except Exception:
                f = StringIO()
                traceback.print_exc(file=f)
                valor = f.getvalue()
                tell('Error NO completed \n\n%s' % valor)
    #
    def on_bt_quit(self, evt):
        self.Destroy()
#
#
#
def get_next_xml(xml):
    """"""
    #
    at_least_one = False
    #
    if os.path.isfile(xml):
        #print "isfile"
        yield xml
    elif os.path.isdir(xml):
        #print "isdir"
        for fxml in os.listdir(xml):
            fxml_path = os.path.join(xml, fxml)
            if os.path.isfile(fxml_path) and fxml_path.endswith('.xml'):
                at_least_one = True
                yield fxml_path
        if not at_least_one:
            tell('No compatible files in %s' % xml)
            yield None
#
#
def check_xml(xml, report=True):
    """"""
    more = False
    #
    try:
        et.parse(xml)
    except ExpatError, e:
        text = "...%s produce error\n'%s'" %(xml[-20:], e)
        text_2 = "\nOK to continue with warnings\nCANCEL to skip warnings"
        if report: more = tell(text+text_2, level='fatal')
    else:
        text = '...%s  OK' % xml[-20:]
    #
    return text, more
#
#
def mod_xml(filepath):
    """ """
    old = """Schema-instance" <origfilename>"""
    new = """Schema-instance"><origfilename>"""
    txt = open(filepath).read()
    newtxt = txt.replace(old, new, 1)
    open(filepath, 'w').write(newtxt)
#
#
#
#
if __name__ == "__main__":
    app = wx.App(0)
    xmltool = XmlTools(None)
    xmltool.Show()
    app.MainLoop()

    
