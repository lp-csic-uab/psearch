#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
psrch_canvas (psearch 106)
2 de octubre de 2009
"""
#
import wx
from numpy import sin, cos, pi, array
from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from psrch_dale_funcs import color
#
#
#noinspection PyUnusedLocal,PyArgumentList
class CanvasMain(wx.Panel):
    """"""
    def __init__(self, parent, xlabel='xlab', ylabel='ylab'):
        """"""
        wx.Panel.__init__(self, parent)
        self.SetBackgroundColour("white")
        self.figure = Figure(figsize=(1, 1))   #(x,y) en inches!
        self.axes = self.figure.add_subplot(111)
        #
        self.xlabel = xlabel
        self.ylabel = ylabel
        #
        self.canvas = FigureCanvas(self, -1, self.figure)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        #
        self.SetSizer(self.sizer)
        self.Fit()
        #
        self.xlim = (0, 1)
        self.ylim = (0, 1)
        self.draw()
    #
    def on_refresh(self, evt):
        """"""
        self.axes.figure.clear()
        self.axes = self.figure.add_subplot(111)
        self.draw()
    #
    def draw(self, hold=False, refresh=True):
        """"""
        self.axes.hold(hold)
        self.axes.set_xlabel(self.xlabel, size='small')
        self.axes.set_ylabel(self.ylabel, size='small')
        if not refresh:
            self.axes.set_xlim(self.xlim)
            self.axes.set_ylim(self.ylim)
        self.set_ticksize()
        self.canvas.draw()
    #
    def set_ticksize(self):
        """"""
        tlx = self.axes.get_xticklabels() 
        tlx.extend(self.axes.get_yticklabels() )
        for label in tlx: label.set_fontsize('xx-small')
#
#
class MinimalCanvasPanel(CanvasMain):
    """A parent class for the fdr_tool and frd_frame classes"""
    def __init__(self, parent, xlabel='D_value', ylabel='X_corr'):
                
        CanvasMain.__init__(self, parent, xlabel, ylabel)
    #
    def plot(self, groups, title="", texts="", kolor=None):
        """groups = [[(idx, x, d),(idx, x, d)],[(idx, x, d),(idx, x, d)]]
        First implemented with axes.plot but finally using axes.scatter.
        axes.plot plotted independently target and decoy in 'for lista' and
        didnt keep a reference for the picker: in the second plot it was
        assigning the same indexes.
        Apparently axes.plot was working in xmaldi!?
        """
        self.groups = groups
        self.fkeys = {}
        self.axes.hold(True)
        self.axes.set_title(title[:-2])
        ypos = 0.85
        col = color()
        for text in texts:
            self.figure.text(0.20, ypos, text)
            ypos -= 0.05
        for label, lista in enumerate(self.groups):
            mylabel = str(label)
            mycolor = kolor if kolor else col.next()
            pbx = []
            pby = []
            keys = []
            for k, x, d in lista: 
                keys.append(k)
                pbx.append(x)
                pby.append(d)
            #
            self.fkeys[mylabel] = keys 
            self.axes.plot(pby, pbx, mycolor, label=mylabel, picker=2)
            self.canvas.draw()
    #
    def draw_line(self, x, y, color='k'):
        """"""
        self.axes.plot((0, x),(y, 0), color, linewidth=0.5)
        self.canvas.draw()
    #
    def draw_circle(self, x, y, radius, color='k'):
        """"""
        npoints = 20
        theta = array(range(npoints)) * pi / (npoints-1)
        xx = radius * sin(theta) + x
        yy = radius * cos(theta) + y
        #
        self.axes.plot(xx, yy, color, linewidth=0.5)
        self.canvas.draw()
    #
    def draw_axes(self, x, y, color='k', trisectors=True):
        """"""
        xmin, xmax = self.axes.get_xlim()
        ymin, ymax = self.axes.get_ylim()
        self.axes.plot((x, x), (ymin, ymax), color + '--', linewidth=0.2)
        self.axes.plot((xmin, xmax), (y, y), color + '--', linewidth=0.2)
        
        if trisectors:
            self.axes.plot((x, x), (ymin, y), color, linewidth=0.5)
            self.axes.plot((xmin, x), (y, y), color, linewidth=0.5)
        else:
            self.axes.plot((x, x),(y, ymax), color, linewidth=0.5)
            self.axes.plot((x, xmax), (y, y), color, linewidth=0.5)
            
        self.canvas.draw()
#
#
class MyFrame(wx.Frame):
    def __init__(self, *args, **kargs):
        wx.Frame.__init__(self,*args, **kargs)
        MinimalCanvasPanel(self)



if __name__ == '__main__':
    
    app = wx.PySimpleApp()
    fr = MyFrame(None)
    fr.Show()
    #noinspection PyUnresolvedReferences
    app.MainLoop()