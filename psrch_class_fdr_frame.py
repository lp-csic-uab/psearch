#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
psrch_class_fdr_frame (psearch)
10 de junio de 2009
"""
#
import wx
from time import time
from matplotlib.backends.backend_wx import NavigationToolbar2Wx
from matplotlib.lines import Line2D
from psrch_canvas import MinimalCanvasPanel
from commons.iconic import Iconic
#
#
#noinspection PyArgumentList
class FdrDistribution(MinimalCanvasPanel):
    """"""
    def __init__(self, parent, xlabel='D_value', ylabel='X_corr'):
        MinimalCanvasPanel.__init__(self, parent, xlabel, ylabel)
        self.canvas.mpl_connect('pick_event', self.on_pick)
        self.time = 0
        self.add_toolbar()
    #
    def add_toolbar(self):  
        """"""
        self.toolbar = NavigationToolbar2Wx(self.canvas)
        self.toolbar.SetToolBitmapSize(wx.Size(21, 25))
        self.toolbar.SetMinSize((1100, 31))
        self.toolbar.Realize()
        #
        self.mass = wx.TextCtrl(self.toolbar, pos=(250, 4), size=(130, 22),
                                                           style=wx.TE_READONLY)
        #
        self.toolbar.update()
        #
        sizerh = wx.BoxSizer(wx.HORIZONTAL)
        sizerh.Add(self.toolbar, 0, wx.LEFT | wx.EXPAND)
        self.sizer.Add(sizerh, 0, wx.EXPAND)
        self.Fit()
    #
    def on_pick(self, evt):
        """Report the id of the identifications when mouse clicked.

        if there are points from two different series close to each other, two
        events are produced, if the points are from the same series, <evt.ind>
        is a list with the indexes. In both cases, the output includes '...more'.

        """
        my_time = time()
        more = ''
        if isinstance(evt.artist, Line2D):
            label = evt.artist._label
            #
            if my_time-self.time < 0.1 or len(evt.ind) > 1:
                more = '...more'
            
            index = evt.ind[0]
            self.time = time()         
            #
            try:
                text = self.fkeys[label][index][-20:] + more
            except AttributeError:
                text = "error"
                
            self.mass.SetValue(text)
#
#
class FdrFrame(wx.Frame, Iconic):
    """Frame showing good and bad identification distribution and FDR line.

    This is the class imported.

    """
    def __init__(self, parent, *args, **kargs):
        wx.Frame.__init__(self, parent, *args, **kargs)
        Iconic.__init__(self)
        self.fdr = FdrDistribution(self)
        self.SetSize((400, 400))
        self.Layout()
    #
    def test(self):
        """Use only for test"""
        grupos = [(('tar_1|6|8', 6, 8), ('tar_2|9|6', 9, 6),
                   ('tar_3|4|5', 4, 5), ('tar_4|7.06|8', 7.06, 8),
                   ('tar_5|7|8.06', 7, 8.06)), 
                  (('dec_1|5|3', 5, 3), ('dec_2|7|8', 7, 8),
                   ('dec_3|3|6', 3, 6))]
        titulo = "hola.txt"
        textos = ("target", "decoy")
        self.fdr.plot(grupos, titulo, textos)
    

if __name__ == '__main__':
    
    app = wx.PySimpleApp()
    fr = FdrFrame(None)
    fr.test()
    fr.Show()
    #noinspection PyUnresolvedReferences
    app.MainLoop()