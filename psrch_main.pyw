#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
psrch_main (psearch 160)
20 october 2009

"""
#
import wx
import os
import threading
import psrch_constants as psc
from psrch_class_miframe import MyFrame
from psrch_aux_guis import IsoQ, CaliQ, ScanCycle
from psrch_check_xml import XmlTools
from psrch_txt import INTRO
from psrch_funcs import run_from_xml_raw, run_from_datafile
from psrch_class_fdr_frame import FdrFrame
from psrch_class_fdr_tool import FdrTool
from commons.iconic import Iconic
from commons.warns import tell
from commons.images import LAMA, OMMANI
from psrch_images import ICON
from commons.maniclock import ManiClock
from commons.info import About
#
#
PSEARCH_TITLE = "  THE DATA SCANNER 1.6dev"
#
#
def set_globals():
    """Read and set global values read from config file.

     These values overwrite those imported from psearch_constants.

     """
    for line in open('psearch_config', 'r'):
        if line.startswith('#'): continue
        key, value = line.strip().split('=')
        if ';;' in value: value = value.split(';;')  #for lists
        setattr(psc, key, value)
#
#
#noinspection PyUnusedLocal
class Psearch(MyFrame, Iconic):
    """Main window."""
    def __init__(self, *args, **kwargs):
        kwargs["style"] = wx.DEFAULT_FRAME_STYLE|wx.FULL_REPAINT_ON_RESIZE
        MyFrame.__init__(self, *args, **kwargs)
        Iconic.__init__(self, icon=ICON, bmp=(LAMA, self.bt_lama))
        #
        set_globals()
        self.points = None    #diccio[id]=[xcor, d, klass]
        self.selected = None
        self.classes = []
        self.property = {}
        self.fdr_params = (None, None)      # (#target, #decoy)
        self.use_raw  = self.cbx_raw.IsChecked()
        self.use_file = self.cbx_file.IsChecked()
        #
        self.maniclock = None
        #
        self.get_frame_properties()
        self.Layout()
        #
        self.td_key = psc.CLAVE
        #
        self.xmldef_dir = psc.xmldef_dir
        self.rawdef_dir = psc.rawdef_dir
        self.fxls_dir = psc.def_dir
        self.xdef_dir = psc.def_dir        #dir for excel file
        self.sdef_dir = psc.def_dir        #dir for alternative reagent isotope data
        self.def_dir = psc.def_dir         #basic dir for default data
        #
        self.isocurrent = dict()
        self.ini_isocurrent()              #init isocurrent dict
        #
        self.reagent = 'itraq'
        #
        self.calidata = psc.calidata
        self.linedata = psc.mylinedata
        self.scandata = psc.scandata
        #
        self.fdr_line = psc.mylinedata
        #
        self.tc_main.SetValue(INTRO)
        #
        if os.path.exists(psc.XMLTEST) and os.path.exists(psc.RAWTEST):
            self.xml = psc.XMLTEST
            self.raw = psc.RAWTEST
            self.xls = psc.EXCELFILE
        else:
            pat = "File <%s> and/or <%s> doesn't exist or cannot be loaded"
            text = pat % (psc.XMLTEST, psc.RAWTEST)
            self.statusbar.SetStatusText(text, 0)
            self.xml = ''
            self.raw = ''
            self.xls = ''
        #
        if os.path.exists(psc.FILETEST):
            self.fxls = psc.FILETEST
            self.bt_see_fxls.SetBackgroundColour("green")
            if not self.xls: self.xls = psc.EXCELFILE
        else:
            self.bt_see_fxls.SetBackgroundColour("red")
            text = "File <%s> doesn't exist or cannot be loaded" % psc.FILETEST
            self.statusbar.SetStatusText(text, 0)
            self.fxls = ""
        #
        self.tc_xml.SetValue(self.xml)
        self.tc_raw.SetValue(self.raw)
        self.tc_fxls.SetValue(self.fxls)
        self.tc_xls.SetValue(self.xls)
        #
        color = "green" if os.path.exists(self.xls) else "red"
        self.bt_see_xls.SetBackgroundColour(color)
        #
        self.tc_fxls.Disable()
        #
        self.SetTitle(PSEARCH_TITLE)
        self.tc_cut_fdr.SetValue('1.0')
        #
        self.Bind(wx.EVT_BUTTON, self.on_run, self.bt_run)
        self.Bind(wx.EVT_BUTTON, self.on_lama, self.bt_lama)
        self.Bind(wx.EVT_BUTTON, self.on_xml, self.bt_xml)
        self.Bind(wx.EVT_BUTTON, self.on_raw, self.bt_raw)
        self.Bind(wx.EVT_BUTTON, self.on_fxls, self.bt_fxls)
        self.Bind(wx.EVT_BUTTON, self.on_xls, self.bt_xls)
        self.Bind(wx.EVT_BUTTON, self.on_bt_see_fxls, self.bt_see_fxls)
        self.Bind(wx.EVT_BUTTON, self.on_bt_see_xls, self.bt_see_xls)
        self.Bind(wx.EVT_BUTTON, self.on_bt_iso, self.bt_iso)
        self.Bind(wx.EVT_BUTTON, self.on_bt_cal, self.bt_cal)
        self.Bind(wx.EVT_BUTTON, self.on_bt_fdr, self.bt_fdr)
        self.Bind(wx.EVT_BUTTON, self.on_bt_show, self.bt_show)
        self.Bind(wx.EVT_BUTTON, self.on_bt_tools, self.bt_tools)
        self.Bind(wx.EVT_BUTTON, self.on_bt_rst, self.bt_rst)
        self.Bind(wx.EVT_BUTTON, self.on_bt_scprg, self.bt_scprg)
        self.Bind(wx.EVT_BUTTON, self.on_bt_rgnt, self.bt_rgnt)
        self.Bind(wx.EVT_RADIOBOX, self.on_rbx_oper, self.rbx_oper)
        self.Bind(wx.EVT_CHECKBOX, self.on_cbx_raw, self.cbx_raw)
        self.Bind(wx.EVT_CHECKBOX, self.on_cbx_batch, self.cbx_batch)
        self.Bind(wx.EVT_CHECKBOX, self.on_cbx_file, self.cbx_file)
        self.Bind(wx.EVT_CLOSE, self.on_close_window)
    #
    def get_frame_properties(self):
        """Set widget default values.

        - TextCtrl
        tc_xml, tc_raw, tc_xls, tc_cut_itraq, tc_cut_fdr
        - RadioBoxes
        rbx_oper  
        - Checkboxes
        cbx_batch, cbx_show, cbx_2_3, cbx_filt, cbx_quant, cbx_splt
        cbx_save, cbx_na1, cbx_sfdr, cbx_na2, cbx_sorg 

        """
        checkboxes = ['cbx_', 'IsChecked', 'batch', '2_3', 'filt',
                      'quant', 'split', 'slog', 'sorg'] 
        textctrls = ['tc_', 'GetValue', 'xml', 'raw', 'fxls', 'xls', 'cut_fdr']
        radioboxes = ['rbx_', 'GetSelection', 'oper']
        for group in [checkboxes, textctrls, radioboxes]:
            prefix = group[0]
            function = group[1]
            for postfix in group[2:]:
                name = prefix + postfix
                #noinspection PyCallingNonCallable
                self.property[postfix] = getattr(getattr(self, name), function)()
    #
    def ini_isocurrent(self):
        """Reads user defaults from <def_dir> folder.

        User defaults are stored in default.itraq and default.tmt files. If
        these files do not exist, defaults are set from application constants
        iso_itq and iso_tmt.

        """
        #
        data_itq = []
        data_tmt = []
        #
        try:
            for line in open(self.def_dir + os.sep + 'default.itraq', 'r'):
                row = [float(item) for item in line.split()]
                data_itq.append(row)
            self.isocurrent['itraq'] = ['user default', data_itq]
        except IOError:
            tell('No user itraq isotope file found. Using internal reference')
            self.isocurrent['itraq'] = ['internal ref', psc.iso_itq]
        #
        try:
            for line in open(self.def_dir + os.sep + 'default.tmt', 'r'):
                row = [float(item) for item in line.split()]
                data_tmt.append(row)
            self.isocurrent['tmt'] = ['user default', data_tmt]
        except IOError:
            tell('No user tmt isotope file found. Using internal reference')
            self.isocurrent['tmt'] = ['internal ref', psc.iso_tmt]
    #
    def revisa(self, arch, direct):
        """Builds full path for filenames cut with get_shortcut()"""
        if arch.startswith('..\\'):
            return direct + os.sep + os.path.split(arch)[-1]
        else:
            return arch
    #
    #todo:needs to add what the isotopic distribution used is ?
    def on_run(self, event):
        """"""
        self.get_frame_properties()
        #
        if self.use_file:
            target = run_from_datafile
            raw = self.property['raw']
            fxls = self.property['fxls']
            if fxls: self.fxls = self.revisa(fxls, self.fxls_dir)
            if raw: self.raw = self.revisa(raw, self.rawdef_dir)
        else:
            target = run_from_xml_raw
            xml = self.property['xml']
            raw = self.property['raw']
            xls = self.property['xls']
            #
            if xml: self.xml = self.revisa(xml, self.xmldef_dir)
            if raw: self.raw = self.revisa(raw, self.rawdef_dir)
            if xls: self.xls = self.revisa(xls, self.xdef_dir)
            #
            basedir, arch = os.path.split(self.xls)
            full_excel = os.path.join(basedir,'full_' + arch)
            if xls: self.fxls = self.revisa(full_excel, basedir)
            #
            if not os.path.exists(self.xml):
                tell("dir/file <%s> doesn't exist" % self.xml)
                return
            #
            if self.use_raw and not os.path.exists(self.raw):
                tell("dir/file <%s> doesn't exist" % self.raw)
                return
            #
            self.tc_fxls.SetValue(self.get_shortcut(self.fxls))
            self.bt_see_fxls.SetBackgroundColour("red")
            
        xls = self.property['xls']
        self.xls = self.revisa(xls, self.xdef_dir)
        posx, posy = self.GetPosition()
        position = posx + 16, posy + 35
        #
        self.maniclock = ManiClock(None, title=" OmClock",
                                         gif=OMMANI, pos=position)
        #noinspection PyUnresolvedReferences
        self.maniclock.Show()
        #
        self.bt_see_xls.SetBackgroundColour("red")
        #
        task = threading.Thread(target=target, args=(self,))
        task.start()
    #
    def on_cbx_batch(self, evt):
        """"""
        #
        self.get_frame_properties()
        xml = self.property['xml']
        raw = self.property['raw']
        is_batch = self.cbx_batch.IsChecked()
        self.property['batch'] = is_batch
        #
        if xml: xml = self.revisa(xml, self.xmldef_dir)
        if raw: raw = self.revisa(raw, self.rawdef_dir)
        
        if os.path.isfile(xml) or os.path.isfile(raw):
            if is_batch:
                self.tc_raw.SetValue("")
                self.tc_xml.SetValue("")
        elif os.path.isdir(xml) or os.path.isdir(raw):
            if not is_batch:
                self.tc_raw.SetValue("")
                self.tc_xml.SetValue("")
    #
    def on_xml(self, event):
        """"""
        wildcard = "Bioworks files (*.xml)|*.xml| all (*.*)|*.*"
        if self.property['batch']:
            dlg = wx.DirDialog(None, "Select Dir", defaultPath=self.xmldef_dir,
                           style=wx.OPEN)
        else:
            dlg = wx.FileDialog(None, "Select File", defaultDir=self.xmldef_dir,
                                wildcard = wildcard,
                                style=wx.OPEN)
        #
        if dlg.ShowModal() == wx.ID_OK:
            path  = dlg.GetPath()
            self.xmldef_dir = os.path.dirname(path)
            self.tc_xml.SetValue(self.get_shortcut(path))
    #
    def on_raw(self, event):
        """"""
        wildcard = "Xcalibur files (*.raw)|*.raw| all (*.*)|*.*"
        if self.property['batch']:
            dlg = wx.DirDialog(self, "Select Dir", defaultPath = self.rawdef_dir,
                           style=wx.OPEN)
        else:
            dlg = wx.FileDialog(self, "Select File", defaultDir = self.rawdef_dir,
                            wildcard = wildcard,
                            style=wx.OPEN)
        #
        if dlg.ShowModal() == wx.ID_OK:
            path  = dlg.GetPath()
            self.rawdef_dir = os.path.dirname(path)
            self.tc_raw.SetValue(self.get_shortcut(path))
    #
    def on_fxls(self, evt):
        """"""
        wildcard = "Result files (*.xls)|*.xls| all (*.*)|*.*"
        dlg = wx.FileDialog(self, "Select File", defaultDir = self.fxls_dir,
                            wildcard = wildcard,
                            style=wx.OPEN)
        
        if dlg.ShowModal() == wx.ID_OK:
            path  = dlg.GetPath()
            #
            color = "green" if os.path.exists(path) else "red"
            self.bt_see_fxls.SetBackgroundColour(color)
            #
            self.fxls_dir = os.path.dirname(path)
            self.tc_fxls.SetValue(self.get_shortcut(path))
    #
    def on_xls(self, event): # wxGlade: MyFrame.<event_handler>
        """"""
        wildcard = "Excel (tab separated)(.xls)|*.xls|All(*.*)|*.*"
        dlg = wx.FileDialog(self, "Select File", defaultDir = self.xdef_dir,
                            wildcard = wildcard,
                            style=wx.SAVE|wx.OVERWRITE_PROMPT)
        
        if dlg.ShowModal() == wx.ID_OK:
            path  = dlg.GetPath()
            #
            color = "green" if os.path.exists(path) else "red"
            self.bt_see_xls.SetBackgroundColour(color)
            #
            self.xdef_dir = os.path.dirname(path)
            self.tc_xls.SetValue(self.get_shortcut(path))
    #
    def get_shortcut(self, prop):
        """Eliminates path levels up to the two higher ones"""
        path_list = prop.split('\\')
        if len(path_list) > 2:
            return r"..\\" + path_list[-2] + os.sep + path_list[-1]
        else:
            return prop
    #
    def on_bt_see_xls(self, evt):
        """Open Excel"""
        #look at properties to catch a file name manual change
        self.get_frame_properties()
        xls = self.property['xls']
        self.xls = self.revisa(xls, self.xdef_dir)
        if os.path.exists(self.xls):
            self.bt_see_xls.SetBackgroundColour("green")
            text = "Reading file <%s>" %self.xls
            self.statusbar.SetStatusText(text, 0)
            os.startfile(self.xls)
        else:
            text = "File <%s> doesn't exist or can not be loaded" % self.xls
            self.statusbar.SetStatusText(text, 0)
            self.bt_see_xls.SetBackgroundColour("red")
    #
    def on_bt_see_fxls(self, evt):
        """Open excel with full results"""
        #look at properties to catch a file name manual change
        self.get_frame_properties()
        fxls = self.property['fxls']
        self.fxls = self.revisa(fxls, self.fxls_dir)
        if os.path.exists(self.fxls):
            self.bt_see_fxls.SetBackgroundColour("green")
            text = "Reading file <%s>" % self.fxls
            self.statusbar.SetStatusText(text, 0)
            os.startfile(self.fxls)
        else:
            text = "File <%s> doesn`t exist or can not be loaded" % self.fxls
            self.statusbar.SetStatusText(text, 0)
            self.bt_see_fxls.SetBackgroundColour("red")
    #
    def on_rbx_oper(self, evt):
        """"""
        self.property['oper'] = self.rbx_oper.GetSelection()
    #
    def on_bt_iso(self, evt):
        """Opens the frame with reporter isotopic distributions.
        """
        isorigin, isodata = self.isocurrent[self.reagent]
        isoframe = IsoQ(self, self.reagent, isodata, isorigin)
        isoframe.Show()
    #
    def on_bt_cal(self, evt):
        """Opens frame with reporter ion masses and allowed desviation
        """
        califrame = CaliQ(self, self.calidata)
        califrame.Show()
    #
    def on_bt_fdr(self, evt):
        """Opens frame with FRD line and parameters
        """
        params = self.fdr_params if all(self.fdr_params) else None 
        line = self.fdr_line if any(self.fdr_line) else None 
        #
        self.fdrframe = FdrTool(self, self.points, line, params)
        self.Bind(wx.EVT_BUTTON, self.on_bt_apply, self.fdrframe.bt_apply)
    #
    def on_bt_scprg(self, evt):
        """"""
        smodeframe = ScanCycle(self, self.scandata)
        smodeframe.Show()
    #
    def on_bt_apply(self, evt):
        """"""
        self.fdr_line = self.fdrframe.get_line_data()
        self.lb_tfdr.SetLabel('custom')
    #
    def on_bt_rst(self, evt):
        """"""
        self.fdr_line = psc.mylinedata
        self.lb_tfdr.SetLabel('is auto')
    #
    def on_bt_rgnt(self, evt):
        """Sets the reagent used. TMT or iTRAQ"""
        self.reagent = 'itraq' if  self.lb_rgnt.Label == 'tmt' else 'tmt'
        self.lb_rgnt.SetLabel(self.reagent)
    #
    def show(self):
        self.on_bt_show('evt')
    #
    def on_bt_show(self, evt):
        """"""
        self.get_frame_properties()
        picture = FdrFrame(self)
        #
        datum = self.selected if self.property['filt'] else self.points
        if not datum: return
        
        groups = []
        for clase in ['tar', 'dec']:
            points = [(k, v[0], v[1]) for k, v in datum.iteritems() if v[2]==clase]
            groups.append(points)
        #
        #set up texts for the picture
        #noinspection PyTupleAssignmentBalance
        ntar, ndec = self.fdr_params
        start, bound, adjust, extra = self.fdr_line
        title = ""
        TXT1 = '\n D =  %.2f    Xcor =  %.2f   '
        TXT2 = '#target =  %i    #decoy =  %i'
        texts = [TXT1 %start, TXT2 %(ntar, ndec)]
        #
        if self.property['split']:
            share = {'sharex':picture.fdr.axes,
                     'sharey':picture.fdr.axes
                     }
            picture_2 = FdrFrame(self, share)
            picture.fdr.plot([groups[0]], title, texts, kolor='b.')
            picture.fdr.draw_line(start[0], start[1])
            picture_2.fdr.plot([groups[1]], title, texts, kolor='r.')
            picture_2.fdr.draw_line(start[0], start[1])
            picture.Show()
            picture_2.Show()
        else:
            picture.fdr.plot(groups, title, texts)
            picture.fdr.draw_line(start[0], start[1])
            picture.Show()
    #
    def on_cbx_raw(self, evt):
        """"""
        self.use_raw = self.cbx_raw.IsChecked()
        self.cbx_2_3.SetValue(self.use_raw)
        self.cbx_quant.SetValue(self.use_raw)
        self.cbx_2_3.Enable(self.use_raw)
        self.cbx_quant.Enable(self.use_raw)
    #
    def on_cbx_file(self, evt):
        """"""
        self.use_file = self.cbx_file.IsChecked()
        reverse = not self.use_file
        self.tc_raw.Enable(reverse)
        self.tc_xml.Enable(reverse)
        self.cbx_raw.Enable(reverse)
        self.tc_fxls.Enable(self.use_file)
    #
    def on_lama(self, evt):
        """"""
        if not About.exists:
            self.about = About()
            self.about.Show()
        else:
            self.about.SetFocus()
    #
    def on_bt_tools(self, evt):
        """"""
        xmltools = XmlTools(None)
        xmltools.Show()
    #
    def on_close_window(self, evt):
        """"""
        try:
            self.maniclock.Destroy()
        except AttributeError:
            pass
            
        self.Destroy()
#
#
class MyApp(wx.App):
    def OnInit(self):
        wx.InitAllImageHandlers()
        frame_1 = Psearch(None, -1, "")
        self.SetTopWindow(frame_1)
        frame_1.Show()
        return 1
#
#
if __name__ == "__main__":

    app = MyApp(0)
    app.MainLoop()
