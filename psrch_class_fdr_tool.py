#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
psrch_class_fdr_tool (psearch)
15 june 2009
"""
#
import wx
from psrch_class_basefdr_tool import BaseFdrTool
from commons.iconic import Iconic
from psrch_constants import linedata
from psrch_canvas import MinimalCanvasPanel
from psrch_dale_funcs import count_line, count_circle, count_axes
from psrch_dale_funcs import calc_fdr
#
#
#noinspection PyUnusedLocal,PySimplifyBooleanCheck
class FdrTool(BaseFdrTool, Iconic):
    def __init__(self, parent, points, line, params, *args, **kargs):
        """
        line  #(start, bound, adjust, extra)
        """
        BaseFdrTool.__init__(self, parent, *args, **kargs)
        Iconic.__init__(self)
        self.points = points
        #
        if params is None: params = (float(linedata['dval']), float(linedata['xc']))
        #
        ntar, ndec = params
        #
        fdr = calc_fdr(ntar, ndec)
        self.lb_dpt.SetLabel(str(ntar))
        self.lb_dpd.SetLabel(str(ndec))
        self.lb_dfdr.SetLabel("%.1f" % fdr)
        #
        self.default_data = linedata.copy()
        self.init_linedata(linedata, line)
        #
        self.panel_1 = MinimalCanvasPanel(self)
        self.SetMinSize((400,500))
        
        self.Bind(wx.EVT_BUTTON, self.on_show, self.bt_shw)
        self.Bind(wx.EVT_BUTTON, self.on_refresh, self.bt_rfrsh)
        self.Bind(wx.EVT_BUTTON, self.on_defaults, self.bt_dflt)
        self.Bind(wx.EVT_BUTTON, self.on_exit, self.bt_exit)
        #
        self._do_layout()
        self.init_points()
        self.init_line(line)
        self.Show()
    #
    def get_groups(self):
        """"""
        groups = []
        for klass in ['tar', 'dec']:
            points =[(k, v[0], v[1]) for k, v in self.points.iteritems() if v[2]==klass]
            groups.append(points)
        
        return groups
    #
    def on_refresh(self, evt):
        self.panel_1.on_refresh(None)
        self.init_points()
    #
    def on_show(self, evt):
        """"""
        rbx = self.rbx_bound.GetSelection()
        if rbx == 0:
            self.do_line()
        elif rbx == 1:
            self.do_circle()
        elif rbx == 2:
            self.do_axes()
    #
    def do_line(self):
        """"""
        #
        x = float(self.tc_dval.GetValue())
        y = float(self.tc_xc.GetValue())
        #
        if x == 0: x = 0.0001
        if y == 0: y = 0.0001
        #
        self.panel_1.draw_line(x, y, 'y')
        # 
        if self.points:
            count = count_line(x, y, self.points)
            self.write_labels(count)
    #
    def do_circle(self):
        """"""
        #
        try:
            x = float(self.tc_cxc.GetValue())
            y = float(self.tc_cyc.GetValue())
            r = float(self.tc_crad.GetValue())
        except ValueError:
            return
        
        self.panel_1.draw_circle(x, y, r, 'y')
        #
        if self.points:
            count = count_circle(x, y, self.points, r)
            self.write_labels(count)
    #
    def do_axes(self):
        """"""
        #
        try:
            y = float(self.tc_axc.GetValue())
            x = float(self.tc_adval.GetValue())
        except ValueError:
            return
        #
        trisectors = self.cbx_a3s.IsChecked()
        self.panel_1.draw_axes(x, y, 'y', trisectors)
        #
        if self.points:
            count = count_axes(x, y, self.points, trisectors)
            self.write_labels(count)
    #
    def write_labels(self, count):
        """"""
        bads = count['dec']
        goods = count['tar']
        fdr = calc_fdr(goods, bads)
        #
        self.lb_dpt.SetLabel(str(goods))
        self.lb_dpd.SetLabel(str(bads))
        self.lb_dfdr.SetLabel('%.1f' % fdr)
    #
    def get_line_data(self):
        """"""
        starters = [(self.tc_dval.GetValue(), self.tc_xc.GetValue()), 
                    (self.tc_cyc.GetValue(), self.tc_cxc.GetValue()),
                    (self.tc_adval.GetValue(), self.tc_axc.GetValue())]
        #
        bound = self.rbx_bound.GetSelection()   #boundary: lineal(0)/circle(1)/axes(2)
        adjust = self.rbx_set.GetSelection()    #adjust: fix(0)/as_start(1)
        crad = self.tc_crad.GetValue()          #radius
        a3s = self.cbx_a3s.IsChecked()          #sectors: 1 quadrant(0)/3 quadrants(1)
        #
        additionals = [None, float(crad), a3s]
        # 
        start = (float(starters[bound][0]), float(starters[bound][1]))
        additional = additionals[bound]
                                  
        return start, bound, adjust, additional
    #
    def on_defaults(self, evt):
        """"""
        self.init_linedata(self.default_data)
    #
    def init_linedata(self, data, line=None):
        """"""
        self.rbx_bound.SetSelection(data['bound'])   #boundary: lineal(0)/circle(1)/axes(2)
        self.rbx_set.SetSelection(data['adjust'])    #adjust: fix(0)/as_start(1)
        self.tc_xc.SetValue(data['xc'])              #initial xc cutoff
        self.tc_dval.SetValue(data['dval']) 
        self.tc_cxc.SetValue(data['cxc'])
        self.tc_cyc.SetValue(data['cyc'])
        self.tc_axc.SetValue(data['axc'])
        self.tc_adval.SetValue(data['adval'])
        self.tc_crad.SetValue(data['crad'])         #radius
        self.cbx_a3s.SetValue(data['a3s'])          #sectors: 1 quadrant(0)/3 quadrants(1)
        
        if not line: return
        
        x = str(line[0][0])
        y = str(line[0][1])
        z = str(line[3])
        
        if line[1] == 0:
            self.tc_xc.SetValue(y)             #points de corte inicial
            self.tc_dval.SetValue(x) 
        elif line[1] == 1:
            self.tc_cxc.SetValue(x)
            self.tc_cyc.SetValue(y)
            self.tc_crad.SetValue(z)
        elif line[1] == 2:
            self.tc_axc.SetValue(y)
            self.tc_adval.SetValue(x)
            self.cbx_a3s.SetValue(line[3])
            
        self.rbx_bound.SetSelection(line[1])
        self.rbx_set.SetSelection(line[2])
    #
    def init_points(self):
        """Calculates total target and decoy"""
        if self.points is None: return
        #
        grupos = self.get_groups()
        ttarget = len(grupos[0])
        tdecoy = len(grupos[1])
        
        self.lb_dtt.SetLabel(str(ttarget))
        self.lb_dtd.SetLabel(str(tdecoy))
        self.panel_1.plot(grupos)
    #
    #noinspection PyCallingNonCallable
    def init_line(self, line):
        """"""
        draws = ['do_line', 'do_circle', 'do_axes']
        draw = draws[line[1]]
        getattr(self, draw)()
    #
    def on_exit(self, evt):
        self.Destroy()
    

if __name__ == '__main__':
    
    line = ((10,10), 1, 1, 2)
    puntos = dict(p0 = (1, -1, 'tar'),
                  p1 = (2, 3, 'tar'),
                  p2 = (7, 8, 'tar'),
                  p3 = (4, 5, 'dec'),
                  p4 = (9, 10, 'dec')
                )
    app = wx.PySimpleApp()
    fr = FdrTool(None, puntos, line, None)
    fr.Show()
    #noinspection PyUnresolvedReferences
    app.MainLoop()