#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
# generated by wxGlade 0.6.2 on Wed Sep 09 23:51:00 2009
#
# Needs refactoring -> xls <-> fxls, bcht -> batch,
#                      sifxls, sixls -> see_....
#                      cfffdr -> cut_fdr
#
import wx

# begin wxGlade: extracode
# end wxGlade



class MyFrame(wx.Frame):
    #noinspection PyArgumentList
    def __init__(self, *args, **kwds):
        # begin wxGlade: MyFrame.__init__
        kwds["style"] = wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        self.statusbar = self.CreateStatusBar(1, 0)
        self.tc_main = wx.TextCtrl(self, wx.ID_ANY, "\n                                  The Shotgun Processor", style=wx.TE_MULTILINE | wx.TE_READONLY)
        self.lb_xml = wx.StaticText(self, wx.ID_ANY, "XML       ")
        self.tc_xml = wx.TextCtrl(self, wx.ID_ANY, "")
        self.bt_xml = wx.Button(self, wx.ID_ANY, "...")
        self.lb_raw = wx.StaticText(self, wx.ID_ANY, "RAW      ")
        self.tc_raw = wx.TextCtrl(self, wx.ID_ANY, "")
        self.bt_raw = wx.Button(self, wx.ID_ANY, "...")
        self.lb_fxls = wx.StaticText(self, wx.ID_ANY, "FILE  ")
        self.tc_fxls = wx.TextCtrl(self, wx.ID_ANY, "")
        self.bt_fxls = wx.Button(self, wx.ID_ANY, "...")
        self.bt_see_fxls = wx.Button(self, wx.ID_ANY, "")
        self.lb_xls = wx.StaticText(self, wx.ID_ANY, "REPORT  ")
        self.tc_xls = wx.TextCtrl(self, wx.ID_ANY, "")
        self.bt_xls = wx.Button(self, wx.ID_ANY, "...")
        self.bt_see_xls = wx.Button(self, wx.ID_ANY, "")
        self.rbx_oper = wx.RadioBox(self, wx.ID_ANY, "fdr line calculation", choices=["single", "per_file"], majorDimension=1, style=wx.RA_SPECIFY_ROWS)
        self.cbx_raw = wx.CheckBox(self, wx.ID_ANY, "use raw")
        self.cbx_file = wx.CheckBox(self, wx.ID_ANY, "use file")
        self.bt_run = wx.Button(self, wx.ID_ANY, "RUN")
        self.bt_lama = wx.BitmapButton(self, wx.ID_ANY, wx.NullBitmap, style=wx.BU_AUTODRAW)
        self.cbx_batch = wx.CheckBox(self, wx.ID_ANY, "bacht")
        self.tc_sec = wx.TextCtrl(self, wx.ID_ANY, "", style=wx.TE_MULTILINE | wx.TE_READONLY)
        self.lb_filt = wx.StaticText(self, wx.ID_ANY, "filtering", style=wx.ALIGN_CENTRE)
        self.tc_cut_fdr = wx.TextCtrl(self, wx.ID_ANY, "10")
        self.lb_cut_fdr = wx.StaticText(self, wx.ID_ANY, " fdr max")
        self.bt_fdr = wx.Button(self, wx.ID_ANY, "fdr_line")
        self.lb_tfdr = wx.StaticText(self, wx.ID_ANY, "is auto", style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
        self.bt_rst = wx.Button(self, wx.ID_ANY, "x")
        self.lb_rgnt = wx.StaticText(self, wx.ID_ANY, "  itraq", style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
        self.bt_rgnt = wx.Button(self, wx.ID_ANY, "x")
        self.bt_iso = wx.Button(self, wx.ID_ANY, "isotopes")
        self.bt_cal = wx.Button(self, wx.ID_ANY, "calibrate")
        self.bt_scprg = wx.Button(self, wx.ID_ANY, "scan prog")
        self.lb_graph = wx.StaticText(self, wx.ID_ANY, "graphs", style=wx.ALIGN_CENTRE)
        self.lb_proc = wx.StaticText(self, wx.ID_ANY, "process", style=wx.ALIGN_CENTRE)
        self.cbx_filt = wx.CheckBox(self, wx.ID_ANY, "only_filtered")
        self.cbx_2_3 = wx.CheckBox(self, wx.ID_ANY, "add_ms2_ms3")
        self.cbx_split = wx.CheckBox(self, wx.ID_ANY, "split t-d")
        self.cbx_quant = wx.CheckBox(self, wx.ID_ANY, "quant iTRAQ")
        self.bt_show = wx.Button(self, wx.ID_ANY, "show_graph")
        self.lb_save = wx.StaticText(self, wx.ID_ANY, "save", style=wx.ALIGN_CENTRE)
        self.lb_tools = wx.StaticText(self, wx.ID_ANY, "tools", style=wx.ALIGN_CENTRE)
        self.cbx_slog = wx.CheckBox(self, wx.ID_ANY, "save_log")
        self.bt_tools = wx.Button(self, wx.ID_ANY, " xml tools")
        self.cbx_sorg = wx.CheckBox(self, wx.ID_ANY, "save_original")

        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_BUTTON, self.on_run, self.bt_run)
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: MyFrame.__set_properties
        self.SetTitle("Data Process 07")
        self.SetBackgroundColour(wx.Colour(253, 255, 164))
        self.statusbar.SetStatusWidths([-1])
        # statusbar fields
        statusbar_fields = ["frame_1_statusbar"]
        for i in range(len(statusbar_fields)):
            self.statusbar.SetStatusText(statusbar_fields[i], i)
        self.tc_main.SetMinSize((450, 150))
        self.bt_xml.SetMinSize((40, 21))
        self.bt_raw.SetMinSize((40, 21))
        self.bt_fxls.SetMinSize((25, 21))
        self.bt_see_fxls.SetMinSize((16, 16))
        self.bt_xls.SetMinSize((25, 21))
        self.bt_see_xls.SetMinSize((16, 16))
        self.rbx_oper.SetSelection(0)
        self.cbx_raw.SetValue(1)
        self.bt_lama.SetBitmapLabel(wx.NullBitmap)
        self.bt_lama.SetSize(self.bt_lama.GetBestSize())
        self.cbx_batch.SetValue(1)
        self.tc_sec.SetMinSize((450, 75))
        self.lb_filt.SetBackgroundColour(wx.Colour(225, 255, 106))
        self.tc_cut_fdr.SetMinSize((30, 20))
        self.bt_fdr.SetMinSize((75, 18))
        self.lb_tfdr.SetMinSize((55, 15))
        self.lb_tfdr.SetBackgroundColour(wx.Colour(255, 195, 42))
        self.bt_rst.SetMinSize((18, 16))
        self.lb_rgnt.SetMinSize((48, 16))
        self.lb_rgnt.SetBackgroundColour(wx.Colour(255, 195, 42))
        self.bt_rgnt.SetMinSize((16, 16))
        self.bt_iso.SetMinSize((66, 20))
        self.bt_cal.SetMinSize((65, 20))
        self.bt_scprg.SetMinSize((65, 20))
        self.lb_graph.SetBackgroundColour(wx.Colour(255, 255, 0))
        self.lb_proc.SetMinSize((130, 16))
        self.lb_proc.SetBackgroundColour(wx.Colour(255, 255, 0))
        self.cbx_filt.SetValue(1)
        self.cbx_2_3.SetValue(1)
        self.cbx_quant.SetValue(1)
        self.bt_show.SetMinSize((77, 15))
        self.lb_save.SetBackgroundColour(wx.Colour(255, 255, 0))
        self.lb_tools.SetBackgroundColour(wx.Colour(255, 255, 0))
        self.cbx_slog.SetValue(1)
        self.bt_tools.SetMinSize((80, 20))
        self.cbx_sorg.SetValue(1)
        # end wxGlade

    #noinspection PyArgumentList
    def __do_layout(self):
        # begin wxGlade: MyFrame.__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_10 = wx.BoxSizer(wx.HORIZONTAL)
        grid_sizer_1 = wx.GridSizer(6, 2, 0, 0)
        sizer_12 = wx.BoxSizer(wx.VERTICAL)
        sizer_18 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_15 = wx.BoxSizer(wx.VERTICAL)
        sizer_16 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_11 = wx.BoxSizer(wx.VERTICAL)
        sizer_4 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_14 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_13 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_17 = wx.BoxSizer(wx.VERTICAL)
        sizer_8 = wx.BoxSizer(wx.VERTICAL)
        sizer_9 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_3 = wx.BoxSizer(wx.VERTICAL)
        sizer_5 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_20 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_6 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_7 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_1.Add(self.tc_main, 0, wx.ALL | wx.EXPAND, 7)
        label_archiv = wx.StaticText(self, wx.ID_ANY, "Archives", style=wx.ALIGN_CENTRE)
        label_archiv.SetBackgroundColour(wx.Colour(255, 255, 0))
        sizer_1.Add(label_archiv, 0, wx.TOP | wx.BOTTOM | wx.EXPAND, 2)
        sizer_7.Add(self.lb_xml, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_7.Add(self.tc_xml, 1, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_7.Add(self.bt_xml, 0, wx.RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_3.Add(sizer_7, 1, wx.LEFT | wx.EXPAND, 2)
        sizer_6.Add(self.lb_raw, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_6.Add(self.tc_raw, 1, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_6.Add(self.bt_raw, 0, wx.RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_3.Add(sizer_6, 1, wx.LEFT | wx.EXPAND, 2)
        sizer_20.Add(self.lb_fxls, 0, wx.RIGHT | wx.ALIGN_CENTER_VERTICAL, 15)
        sizer_20.Add(self.tc_fxls, 1, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_20.Add(self.bt_fxls, 0, wx.RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_20.Add(self.bt_see_fxls, 0, wx.TOP, 7)
        sizer_3.Add(sizer_20, 1, wx.LEFT | wx.EXPAND, 2)
        sizer_5.Add(self.lb_xls, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_5.Add(self.tc_xls, 1, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_5.Add(self.bt_xls, 0, wx.RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_5.Add(self.bt_see_xls, 0, wx.TOP, 7)
        sizer_3.Add(sizer_5, 1, wx.LEFT | wx.EXPAND, 2)
        sizer_2.Add(sizer_3, 1, wx.EXPAND, 0)
        sizer_2.Add((20, 20), 0, wx.EXPAND, 0)
        sizer_8.Add(self.rbx_oper, 0, wx.TOP | wx.BOTTOM | wx.EXPAND, 2)
        sizer_9.Add(self.cbx_raw, 0, wx.RIGHT | wx.TOP | wx.BOTTOM, 4)
        sizer_9.Add(self.cbx_file, 0, wx.LEFT | wx.TOP | wx.BOTTOM, 4)
        sizer_8.Add(sizer_9, 0, wx.EXPAND, 0)
        sizer_8.Add(self.bt_run, 0, wx.TOP | wx.BOTTOM | wx.EXPAND, 3)
        sizer_2.Add(sizer_8, 0, wx.EXPAND, 0)
        sizer_17.Add(self.bt_lama, 1, wx.ALL | wx.EXPAND, 5)
        sizer_17.Add(self.cbx_batch, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 8)
        sizer_2.Add(sizer_17, 0, wx.EXPAND, 0)
        sizer_1.Add(sizer_2, 0, wx.EXPAND, 0)
        sizer_1.Add(self.tc_sec, 1, wx.EXPAND, 0)
        lb_par = wx.StaticText(self, wx.ID_ANY, "parameters", style=wx.ALIGN_CENTRE)
        lb_par.SetBackgroundColour(wx.Colour(255, 255, 0))
        sizer_12.Add(lb_par, 0, wx.ALL | wx.EXPAND, 2)
        sizer_18.Add((20, 20), 0, wx.EXPAND, 0)
        sizer_11.Add(self.lb_filt, 1, wx.RIGHT | wx.TOP | wx.BOTTOM | wx.EXPAND | wx.ALIGN_CENTER_HORIZONTAL, 2)
        sizer_13.Add(self.tc_cut_fdr, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 1)
        sizer_13.Add(self.lb_cut_fdr, 0, wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 2)
        sizer_11.Add(sizer_13, 1, wx.EXPAND, 0)
        sizer_14.Add(self.bt_fdr, 0, wx.TOP, 2)
        sizer_14.Add((20, 18), 1, wx.EXPAND, 0)
        sizer_11.Add(sizer_14, 1, wx.EXPAND, 0)
        sizer_4.Add(self.lb_tfdr, 0, wx.TOP | wx.EXPAND, 7)
        sizer_4.Add(self.bt_rst, 0, wx.TOP | wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_4.Add((20, 20), 1, wx.EXPAND, 0)
        sizer_11.Add(sizer_4, 1, wx.EXPAND, 0)
        sizer_18.Add(sizer_11, 0, 0, 0)
        sizer_18.Add((25, 20), 0, wx.EXPAND, 0)
        sizer_16.Add(self.lb_rgnt, 0, wx.LEFT, 2)
        sizer_16.Add(self.bt_rgnt, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_15.Add(sizer_16, 0, 0, 0)
        sizer_15.Add(self.bt_iso, 0, wx.ALL, 2)
        sizer_15.Add(self.bt_cal, 0, wx.ALL, 2)
        sizer_15.Add(self.bt_scprg, 0, wx.ALL, 2)
        sizer_18.Add(sizer_15, 0, wx.EXPAND, 0)
        sizer_18.Add((20, 20), 0, wx.EXPAND, 0)
        sizer_12.Add(sizer_18, 0, wx.EXPAND, 0)
        sizer_10.Add(sizer_12, 0, wx.ALL | wx.EXPAND, 4)
        grid_sizer_1.Add(self.lb_graph, 0, wx.ALL | wx.EXPAND | wx.ALIGN_CENTER_HORIZONTAL, 2)
        grid_sizer_1.Add(self.lb_proc, 0, wx.ALL | wx.EXPAND | wx.ALIGN_CENTER_HORIZONTAL, 2)
        grid_sizer_1.Add(self.cbx_filt, 0, wx.LEFT, 20)
        grid_sizer_1.Add(self.cbx_2_3, 0, wx.LEFT, 20)
        grid_sizer_1.Add(self.cbx_split, 0, wx.LEFT, 20)
        grid_sizer_1.Add(self.cbx_quant, 0, wx.LEFT, 20)
        grid_sizer_1.Add(self.bt_show, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)
        grid_sizer_1.Add(self.lb_save, 0, wx.ALL | wx.EXPAND | wx.ALIGN_CENTER_HORIZONTAL, 2)
        grid_sizer_1.Add(self.lb_tools, 0, wx.ALL | wx.EXPAND | wx.ALIGN_CENTER_HORIZONTAL, 2)
        grid_sizer_1.Add(self.cbx_slog, 0, wx.LEFT, 20)
        grid_sizer_1.Add(self.bt_tools, 0, wx.ALIGN_CENTER_HORIZONTAL, 10)
        grid_sizer_1.Add(self.cbx_sorg, 0, wx.LEFT, 20)
        sizer_10.Add(grid_sizer_1, 0, wx.RIGHT | wx.TOP | wx.BOTTOM, 4)
        sizer_1.Add(sizer_10, 0, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        sizer_1.Fit(self)
        self.Layout()
        # end wxGlade

    def on_run(self, event): # wxGlade: MyFrame.<event_handler>
        print "Event handler `on_run' not implemented!"
        event.Skip()

# end of class MyFrame


class MyApp(wx.App):
    def OnInit(self):
        wx.InitAllImageHandlers()
        frame_1 = MyFrame(None, -1, "")
        self.SetTopWindow(frame_1)
        frame_1.Show()
        return 1

# end of class MyApp

if __name__ == "__main__":
    app = MyApp(0)
    app.MainLoop()
